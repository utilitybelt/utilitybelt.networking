﻿param([string]$SolutionDir,
     [string]$ProjectDir,
     [string]$ProjectPath,
     [string]$ConfigurationName,
     [string]$TargetName,
     [string]$TargetDir,
     [string]$ProjectName,
     [string]$PlatformName,
     [string]$NuGetPackageRoot,
     [string]$TargetPath);
     
If ($Env:OS -ne "" -and $Env:OS -ne $null -and $Env:OS.ToLower().Contains("windows")) {
    Remove-Item -LiteralPath "${NuGetPackageRoot}utilitybelt.networking" -Force -Recurse
    Remove-Item -Path "${SolutionDir}bin\Release\*.nupkg" -Force -Recurse
}