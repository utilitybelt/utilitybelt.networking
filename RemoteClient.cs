﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using UtilityBelt.Networking.Lib;
using ProtoBuf;

namespace UtilityBelt.Networking {
    /// <summary>
    /// A remotely connected client
    /// </summary>
    public class RemoteClient {
        /// <summary>
        /// The id of the remote client
        /// </summary>
        public uint Id { get; internal set; }

        /// <summary>
        /// The friendly name of the client
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// The type of client this is
        /// </summary>
        public ClientType Type { get; internal set; }

        /// <summary>
        /// The tags this client belongs to.
        /// </summary>
        public List<string> Tags { get; internal set; } = new List<string>();

        /// <summary>
        /// A list of string key/value pairs. you can store custom data here.
        /// </summary>
        public Dictionary<string, string> Properties { get; internal set; } = new Dictionary<string, string>();

        internal RemoteClient(uint id, ClientType type, string name) {
            Id = id;
            Type = type;
            Name = name;
        }

        public override string ToString() {
            return $"{this.GetType().Name}:{Type}({Id})<{Name}>";
        }
    }
}
