﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Networking {
    /// <summary>
    /// UBNetClient options
    /// </summary>
    public class UBNetClientOptions {
        /// <summary>
        /// The host to connect to
        /// </summary>
        public string Host { get; } = "127.0.0.1";

        /// <summary>
        /// The port to connect to
        /// </summary>
        public int Port { get; } = 42163;

        /// <summary>
        /// Creates a new options instance
        /// </summary>
        /// <param name="host">The UBNet server host to connect to</param>
        /// <param name="port">The port to use when connecting to UBNet</param>
        public UBNetClientOptions(string host, int port) {
            Host = host;
            Port = port;
        }

        public UBNetClientOptions() { }
    }
}
