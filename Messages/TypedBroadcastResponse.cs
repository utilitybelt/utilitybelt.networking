﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages {
    /// <summary>
    /// This is what clients should respond with when getting a TypedBroadcast packet
    /// </summary>
    [ProtoContract]
    public class TypedBroadcastResponse : ResponsePacket {
        /// <summary>
        /// Wether or not the request was successful
        /// </summary>
        [ProtoMember(1)]
        public bool Success { get; set; } = false;

        /// <summary>
        /// The id of the client sending this response.
        /// </summary>
        [ProtoMember(3)]
        public uint ClientId { get; set; }

        /// <summary>
        /// The data generated by the request
        /// </summary>
        [ProtoMember(2)]
        public byte[] Data { get; set; }

        /// <summary>
        /// Create a new TypedBroadcast response
        /// </summary>
        public TypedBroadcastResponse() {
            PacketType = PacketType.TypedBroadcastResponse;

        }

        /// <summary>
        /// Create a new TypedBroadcast response
        /// </summary>
        public TypedBroadcastResponse(uint clientId, bool success, byte[] data, RequestPacket req) : base(req) {
            PacketType = PacketType.TypedBroadcastResponse;
            ClientId = clientId;
            Success = success;
            Data = data;
        }
    }
}
