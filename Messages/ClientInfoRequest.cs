﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class ClientInfoRequest : RequestPacket {
        /// <summary>
        /// The id of the remote client
        /// </summary>
        [ProtoMember(1)]
        public uint ClientId { get; internal set; }

        /// <summary>
        /// The type of client this is
        /// </summary>
        [ProtoMember(2)]
        public ClientType Type { get; internal set; }

        /// <summary>
        /// The friendly name of the client
        /// </summary>
        [ProtoMember(3)]
        public string Name { get; internal set; }

        /// <summary>
        /// The tags this client belongs to.
        /// </summary>
        [ProtoMember(4)]
        public List<string> Tags { get; internal set; } = new List<string>();

        /// <summary>
        /// A list of string key/value pairs. you can store custom data here.
        /// </summary>
        [ProtoMember(5)]
        public Dictionary<string, string> Properties { get; internal set; } = new Dictionary<string, string>();

        public ClientInfoRequest() : base() {
            PacketType = PacketType.ClientInfoRequest;
        }

        public ClientInfoRequest(uint id, ClientType type, string name, List<string> tags, Dictionary<string, string> properties) {
            PacketType = PacketType.ClientInfoRequest;
            ClientId = id;
            Type = type;
            Name = name;
            Tags = tags;
            Properties = properties;
        }
    }
}
