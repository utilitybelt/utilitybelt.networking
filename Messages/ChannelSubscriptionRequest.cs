﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class ChannelSubscriptionRequest : RequestPacket {
        [ProtoMember(1)]
        public string Channel { get; internal set; }

        [ProtoMember(2)]
        public bool Subscribe { get; internal set; }

        public ChannelSubscriptionRequest() : base() {
            PacketType = PacketType.ChannelSubscriptionRequest;

        }

        public ChannelSubscriptionRequest(string channel, bool subscribe) {
            PacketType = PacketType.ChannelSubscriptionRequest;
            Channel = channel;
            Subscribe = subscribe;
        }
    }
}
