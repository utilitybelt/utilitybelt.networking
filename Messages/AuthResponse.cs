﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class AuthResponse : ResponsePacket {
        [ProtoMember(1)]
        public uint ClientId { get; set; }

        public AuthResponse() : base() {
            PacketType = PacketType.AuthResponse;
        }

        public AuthResponse(uint clientId, AuthRequest request) : base(request) {
            PacketType = PacketType.AuthResponse;
            ClientId = clientId;
        }
    }
}
