﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class DummyRequest : RequestPacket {
        public DummyRequest() : base() {
            PacketType = PacketType.DummyRequest;
        }
    }
}
