﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract(SkipConstructor = true)]
    public class AuthRequest : RequestPacket {
        [ProtoMember(1)]
        public string Name { get; set; } = "";

        [ProtoMember(2)]
        public ClientType ClientType { get; set; }

        public AuthRequest() : base() {
            PacketType = PacketType.AuthRequest;
        }

        public AuthRequest(string name, ClientType clientType) : base() {
            PacketType = PacketType.AuthRequest;
            Name = name;
            ClientType = clientType;
        }
    }
}
