﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    public enum ClientEventType : ushort {
        Connected = 1,
        Disconnected = 2,
        Updated = 3
    }

    [ProtoContract]
    public class ClientEvent : IPacket {
        [ProtoMember(1)]
        public PacketType PacketType { get; set; } =  PacketType.ClientEvent;

        [ProtoMember(2)]
        public uint ClientId { get; set; }

        [ProtoMember(3)]
        public ClientEventType EventType { get; set; }

        [ProtoMember(4)]
        public string ClientName { get; set; } = "";

        [ProtoMember(5)]
        public ClientType ClientType { get; set; }

        public ClientEvent() {
            PacketType = PacketType.ClientEvent;
        }

        public ClientEvent(uint clientId, ClientEventType eventType, string name, ClientType clientType) {
            PacketType = PacketType.ClientEvent;
            ClientId = clientId;
            EventType = eventType;
            ClientType = clientType;
            ClientName = name;
        }

        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
