﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class ChannelBroadcast : BroadcastPacket {
        [ProtoMember(1)]
        public string Channel { get; internal set; }

        [ProtoMember(2)]
        public string Message { get; internal set; }

        public ChannelBroadcast() : base() {
            PacketType = PacketType.ChannelBroadcast;
        }

        public ChannelBroadcast(string channel, string message, ClientFilter filter = null) : base(filter) {
            PacketType = PacketType.ChannelBroadcast;
            Channel = channel;
            Message = message;
        }

        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
