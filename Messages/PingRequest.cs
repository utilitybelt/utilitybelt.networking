﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class PingRequest : RequestPacket {
        [ProtoMember(1)]
        public uint Timestamp { get; set; }

        public PingRequest() : base() {
            PacketType = PacketType.PingRequest;
        }

        public PingRequest(uint timestamp) : base() {
            PacketType = PacketType.PingRequest;
            Timestamp = timestamp;
        }
    }
}
