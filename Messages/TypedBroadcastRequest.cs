﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages {
    /// <summary>
    /// A typed broadcast request to the specified clients. This expects a response from each client
    /// matched by the filter.
    /// </summary>
    [ProtoContract]
    public class TypedBroadcastRequest : RequestPacket {
        /// <summary>
        /// The type of message being sent
        /// </summary>
        [ProtoMember(1)]
        public string Type { get; set; }

        /// <summary>
        /// A filter to select which clients to broadcast to
        /// </summary>
        [ProtoMember(2)]
        public ClientFilter ClientFilter { get; set; } = new ClientFilter();

        /// <summary>
        /// Serialized message data.
        /// </summary>
        [ProtoMember(3)]
        public byte[] Data { get; set; }

        /// <summary>
        /// Create a new typed broadcast request
        /// </summary>
        public TypedBroadcastRequest() : base() {
            PacketType = PacketType.TypedBroadcastRequest;
        }

        /// <summary>
        /// Create a new typed broadcast request
        /// </summary>
        /// <param name="type"></param>
        /// <param name="data"></param>
        /// <param name="filter">A filter to choose which clients to send to. If null will send to all other clients.</param>
        public TypedBroadcastRequest(string type, byte[] data, ClientFilter filter = null) : base() {
            PacketType = PacketType.TypedBroadcastRequest;
            if (filter is not null) {
                ClientFilter = filter;
            }
            Type = type;
            Data = data;
        }

        /// <inheritdoc/>
        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
