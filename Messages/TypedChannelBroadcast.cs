﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages {
    [ProtoContract]
    public class TypedChannelBroadcast : BroadcastPacket {
        [ProtoMember(1)]
        public string Channel { get; internal set; }

        [ProtoMember(2)]
        public string Type { get; }

        [ProtoMember(3)]
        public byte[] Data { get; internal set; }

        public TypedChannelBroadcast() : base() {
            PacketType = PacketType.TypedChannelBroadcast;
        }

        public TypedChannelBroadcast(string channel, string type, byte[] data) : base(null) {
            PacketType = PacketType.TypedChannelBroadcast;
            Channel = channel;
            Data = data;
            Type = type;
        }

        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
