﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class PingResponse : ResponsePacket {
        [ProtoMember(1)]
        public uint Timestamp { get; set; }

        public PingResponse() : base() {
            PacketType = PacketType.PingResponse;
        }

        public PingResponse(PingRequest request) : base(request) {
            PacketType = PacketType.PingResponse;
            Timestamp = request.Timestamp;
        }
    }
}
