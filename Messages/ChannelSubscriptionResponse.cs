﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class ChannelSubscriptionResponse : ResponsePacket {
        [ProtoMember(1)]
        public List<string> Channels { get; set; } = new List<string>();

        public ChannelSubscriptionResponse() : base() {
            PacketType = PacketType.ChannelSubscriptionResponse;
        }

        public ChannelSubscriptionResponse(List<string> channels, ChannelSubscriptionRequest request) : base(request) {
            Channels = channels;
            PacketType = PacketType.ChannelSubscriptionResponse;
        }
    }
}
                                                                                                                                                                                                                                                                                                                                  