﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Messages
{
    [ProtoContract]
    public class ClientInfoResponse : ResponsePacket {
        [ProtoMember(1)]
        public uint ClientId { get; set; }

        public ClientInfoResponse() : base() {
            PacketType = PacketType.ClientInfoResponse;
        }

        public ClientInfoResponse(ClientInfoRequest request) : base(request) {
            PacketType = PacketType.ClientInfoResponse;
            ClientId = request.ClientId;
        }
    }
}
