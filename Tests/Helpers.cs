﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Networking.Tests {
    internal static class Helpers {
        public static async Task StartServer(UBNetServer server, ILogger logger) {
            try {
                Assert.IsTrue(await server.Start(), "Could not start server");
            }
            catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail("Could not start server"); }
        }

        public static async Task StartClient(UBNetClient client, ILogger logger) {
            try {
                Assert.IsTrue(await client.Connect(), "Could not connect");
            }
            catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail("Could not connect client"); }
        }
    }
}
