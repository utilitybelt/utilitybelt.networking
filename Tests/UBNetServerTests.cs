﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Tests {
    [TestFixture]
    public class UBNetServerTests {
        private UBNetServerOptions _serverOptions = new UBNetServerOptions();
        private ILogger logger = new Extensions.Logging.NUnit.NUnitLogger("UBNetServerTests", (message, level) => {
            return level >= LogLevel.Warning;
        });

        [Test]
        public void Can_instantiate_new_server() {
            using (var server = new UBNetServer(_serverOptions, logger)) {
                Assert.NotNull(server);
            }
        }

        [Test]
        public async Task Can_start_and_stop_server() {
            using (var server = new UBNetServer(_serverOptions, logger)) {
                await Helpers.StartServer(server, logger);
                Assert.IsTrue(server.IsListening);
                server.Stop();
                Assert.IsFalse(server.IsListening);
            }
        }

        [Test]
        public async Task Server_response_to_auth_request() {
            using (var server = new UBNetServer(_serverOptions, logger)) {
                await Helpers.StartServer(server, logger);
                using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); },  new UBNetClientOptions(_serverOptions.BindIP, _serverOptions.Port), logger)) {
                    await Helpers.StartClient(client, logger);
                    var res = await client.SendRequest<AuthResponse>(new AuthRequest("TESTING 456", ClientType.GameClient));

                    Assert.NotNull(res, "No auth response from the server");
                    Assert.IsTrue(res.ClientId > 0, "Server sent invalid client id");
                }
            }
        }
    }
}
