﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using NUnit.Framework;
using ProtoBuf;
using ProtoBuf.Meta;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Tests {

    [TestFixture]
    public class UBNetClientTests {
        private ILogger logger = new Extensions.Logging.NUnit.NUnitLogger("UBNetClientTests", (message, level) => {
            return level >= LogLevel.Warning;
        });
        private UBNetServer? _server;
        private UBNetClientOptions? _clientOptions;

        [SetUp]
        public async Task BaseSetUp() {
            try {
                _server = new UBNetServer(new UBNetServerOptions(), logger);
                await Helpers.StartServer(_server, logger);
                _clientOptions = new UBNetClientOptions(_server.Options.BindIP, _server.Options.Port);
            }
            catch (Exception ex) { logger?.Log(LogLevel.Error, ex.ToString()); }
        }

        [TearDown]
        public void BaseTearDown() {
            _server?.Dispose();
        }

        [Test]
        public void Can_instantiate_new_client() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions)) {
                Assert.NotNull(client);
            }
        }

        [Test]
        public async Task Can_connect_to_ubnet_tcp() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                Assert.AreEqual(client.IsConnected, true);
            }
        }

        [Test]
        public async Task Can_send_ping_and_get_response() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                uint ts = 123456;
                var req = new PingRequest(ts);
                Assert.AreEqual(ts, req.Timestamp, "Ping Request Timestamp is set incorrectly");
                var pong = await client.SendRequest<PingResponse>(req);
                Assert.AreEqual(ts, pong.Timestamp, "Ping Response Timestamp is set incorrectly");
            }
        }
        
        [Test]
        public async Task Can_send_lots_of_ping_and_get_response_sync() {
            try {
                using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, null)) {
                    await Helpers.StartClient(client, logger);
                    for (var i = 0; i < 100; i++) {
                        uint ts = (uint)i;
                        var req = new PingRequest(ts);
                        Assert.AreEqual(ts, req.Timestamp, "Ping Request Timestamp is set incorrectly");
                        var pong = await client.SendRequest<PingResponse>(req);
                        Assert.AreEqual(ts, pong.Timestamp, "Ping Response Timestamp is set incorrectly");
                    }
                }
            }
            catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail(ex.Message); }
        }

        [Test]
        public async Task Can_send_lots_of_ping_and_get_response_async() {
            try {
                using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, null)) {
                    await Helpers.StartClient(client, logger);
                    var tasks = new List<Task>();
                    for (var i = 0; i < 100; i++) {
                        tasks.Add(new Task(async() => {
                            uint ts = (uint)i;
                            var req = new PingRequest(ts);
                            Assert.AreEqual(ts, req.Timestamp, "Ping Request Timestamp is set incorrectly");
                            var pong = await client.SendRequest<PingResponse>(req, TimeSpan.FromSeconds(5));
                            Assert.NotNull(pong, $"Pong for ts {ts} was null");
                            Assert.AreEqual(ts, pong.Timestamp);
                        }));
                    }

                    foreach (var task in tasks) {
                        task.Start();
                    }
                    
                    await Task.WhenAll(tasks);

                    Assert.IsTrue(tasks.All(t => t.IsCompletedSuccessfully), "Some tasks did not complete successfully");
                }
            }
            catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail(ex.Message); }
        }

        [Test]
        public async Task Can_Update_Name() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                Assert.AreEqual("TestClient", client.Name);
                await client.SetName("New Name");
                Assert.AreEqual("New Name", client.Name, "Client did not update name");
            }
        }

        [Test]
        public async Task Can_Subscribe_toChannel() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                await client.SubscribeToChannel("test");
                Assert.IsTrue(client.Channels.Contains("test"), "Client was not subscribed to test channel");
            }
        }

        [Test]
        public async Task Can_Subscribe_toChannel_and_send_messages_to_itself() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                string messageToSend = "Test Message 123";
                var task = new TaskCompletionSource<string>();
                client.OnChannelMessage += (s, e) => {
                    task.SetResult(e.ChannelBroadcast.Message);
                };
                await client.SubscribeToChannel("test");
                client.PublishToChannel("test", messageToSend);

                var timeout = Task.Delay(1000);
                await Task.WhenAny(timeout, task.Task);

                Assert.IsFalse(timeout.IsCompleted, "timed out, never got channel message");
                Assert.AreEqual(messageToSend, task.Task.Result);
            }
        }

        [Test]
        public async Task Can_Subscribe_toChannel_and_send_messages_and_unsubscribe() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                await client.SubscribeToChannel("test");
                await client.UnsubscribeFromChannel("test");

                Assert.IsFalse(client.Channels.Contains("test"), "Client is still subscribed to test channel");
            }
        }

        [Test]
        public async Task Can_see_other_clients() {
            using (var client1 = new UBNetClient("TestClient1", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger))
            using (var client2 = new UBNetClient("TestClient2", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Task.WhenAll(Helpers.StartClient(client1, logger), Helpers.StartClient(client2, logger));

                await Task.WhenAny(Task.Run(async () => {
                    while (client1.Clients.Count < 1 && client2.Clients.Count < 1) {
                        await Task.Delay(1);
                    }
                }), Task.Delay(1000));

                Assert.IsTrue(client1.Clients.Values.Where(c => c.Id == client2.Id).Any(), "client1 Clients does not contain client2");
                Assert.IsTrue(client2.Clients.Values.Where(c => c.Id == client1.Id).Any(), "client2 Clients does not contain client1");
            }
        }

        [Test]
        public async Task Can_see_lots_of_other_clients() {
            try {
                var clients = new List<UBNetClient>();
                var startTasks = new List<Task>();
                for (var i = 1; i <= 10; i++) {
                    var client = new UBNetClient($"TestClient{i}", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger);
                    clients.Add(client);
                    await Helpers.StartClient(client, logger);
                }

                foreach (var task in startTasks) {
                    Assert.IsTrue(task.IsCompletedSuccessfully, $"Client task failed...");
                }

                var str = new StringBuilder();
                foreach (var client in clients) {
                    if (clients.Count != client.Clients.Count + 1) {
                        var missing = string.Join(", ", clients.Where(c => !client.Clients.ContainsKey(c.Id) && c.Id != client.Id).Select(c => c.Name));
                        str.AppendLine($"Client count for client {client.Name} is different than expected ({client.Clients.Count} vs {startTasks.Count}|{clients.Count}) Missing {missing}.");
                    }
                }

                //Assert.AreEqual(startTasks.Count, client.Clients.Count + 1, $"Client count for client {client.Name} is different than expected.");
                Assert.IsEmpty(str.ToString(), str.ToString());

                foreach (var client in clients) {
                    client.Dispose();
                }
            }
            catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail(ex.Message); }
        }

        [Test]
        public async Task Gets_Connect_events_for_other_clients() {
            using (var client1 = new UBNetClient("TestClient1", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger))
            using (var client2 = new UBNetClient("TestClient2", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                var task = new TaskCompletionSource();
                client1.OnRemoteClientConnected += (s, e) => {
                    if (e.Client.Name == "TestClient2") {
                        task.SetResult();
                    }
                };
                await Helpers.StartClient(client1, logger);
                await Helpers.StartClient(client2, logger);

                await Task.WhenAny(task.Task, Task.Delay(300));

                Assert.IsTrue(task.Task.IsCompleted, "did not get connection event for client 2");
            }
        }

        [Test]
        public async Task Gets_Disconnect_events_for_other_clients() {
            using (var client1 = new UBNetClient("TestClient1", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger))
            using (var client2 = new UBNetClient("TestClient2", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                var task = new TaskCompletionSource();
                client1.OnRemoteClientDisconnected += (s, e) => {
                    if (e.Client.Name == "TestClient2") {
                        task.SetResult();
                    }
                };
                await Helpers.StartClient(client1, logger);
                await Helpers.StartClient(client2, logger);

                client2.Stop();

                await Task.WhenAny(task.Task, Task.Delay(1000));

                Assert.IsTrue(task.Task.IsCompleted, "did not get disconnection event for client 2");
            }
        }

        [Test]
        public async Task Clients_can_send_broadcast_messages_to_eachother() {
            using (var client1 = new UBNetClient("TestClient1", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger))
            using (var client2 = new UBNetClient("TestClient2", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                var task1 = new TaskCompletionSource();
                var task2 = new TaskCompletionSource();

                await Task.WhenAll(Helpers.StartClient(client1, logger), Helpers.StartClient(client2, logger));

                client1.OnChannelMessage += (s, e) => {
                    if (e.ChannelBroadcast.Channel == "test" && e.ChannelBroadcast.Message == "TestClient2")
                        task1.SetResult();
                };
                client2.OnChannelMessage += (s, e) => {
                    if (e.ChannelBroadcast.Channel == "test" && e.ChannelBroadcast.Message == "TestClient1")
                        task2.SetResult();
                };

                await client1.SubscribeToChannel("test");
                await client2.SubscribeToChannel("test");

                client1.PublishToChannel("test", "TestClient1");
                client2.PublishToChannel("test", "TestClient2");

                await Task.WhenAny(Task.WhenAll(task1.Task, task2.Task), Task.Delay(1000));

                Assert.IsTrue(task1.Task.IsCompleted, "client1 did not see message on test channel from client2");
                Assert.IsTrue(task2.Task.IsCompleted, "client2 did not see message on test channel from client1");
            }
        }

        [ProtoContract]
        public class TestMessage {
            [ProtoMember(1)]
            public string Text { get; set; } = "Default Test Text";

            [ProtoMember(2)]
            public uint Number { get; set; } = 1337;

            public TestMessage() {
            
            }

            public TestMessage(string text, uint number) {
                Text = text;
                Number = number;
            }

            public override string ToString() {
                return $"TestMessage<{Number}, {Text}>";
            }
        }

        [Test]
        public async Task Can_Send_Custom_Channel_Message() {
            using (var client1 = new UBNetClient("TestClient1", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger))
            using (var client2 = new UBNetClient("TestClient2", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                try {
                    await Task.WhenAll(Helpers.StartClient(client1, logger), Helpers.StartClient(client2, logger));

                    var testMessage = new TestMessage("Hello World", 123456);

                    var client2Task = new TaskCompletionSource<bool>();
                    client2.OnTypedChannelMessage += (s, e) => {
                        if (e.IsType(typeof(TestMessage)) && e.TryDeserialize(out TestMessage message)) {
                            Assert.AreEqual(testMessage.Text, message.Text, "Deserialized text differed");
                            Assert.AreEqual(testMessage.Number, message.Number, "Deserialized number differed");

                            client2Task.SetResult(true);
                        }
                        else {
                            client2Task.SetResult(false);
                        }
                    };

                    client1.PublishToChannel("test", testMessage);

                    await Task.WhenAny(client2Task.Task, Task.Delay(100));

                    Assert.IsTrue(client2Task.Task.IsCompleted, "Client2 didnt get message");
                }
                catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail(ex.Message); }
            }
        }


        [ProtoContract]
        public class TestMessageRequest {
            [ProtoMember(1)]
            public string Text { get; set; } = "Default Test Text";

            [ProtoMember(2)]
            public uint Number { get; set; } = 1337;

            public TestMessageRequest() {

            }

            public TestMessageRequest(string text, uint number) {
                Text = text;
                Number = number;
            }

            public override string ToString() {
                return $"TestMessageRequest<{Number}, {Text}>";
            }
        }

        [ProtoContract]
        public class TestMessageResponse {
            [ProtoMember(1)]
            public string ResponseText { get; set; } = "Default Test Text";

            public TestMessageResponse() : base() {

            }

            public TestMessageResponse(string text){
                ResponseText = text;
            }

            public override string ToString() {
                return $"TestMessageResponse<{ResponseText}>";
            }
        }

        [Test]
        public async Task Can_send_typed_broadcast_and_get_responses() {
            try {
                var clients = new List<UBNetClient>();
                var startTasks = new List<Task>();
                for (var i = 1; i <= 10; i++) {
                    var client = new UBNetClient($"TestClient{i}", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger);
                    clients.Add(client);
                    var task = Helpers.StartClient(client, logger);
                    startTasks.Add(task);
                    await task;
                }

                await Task.WhenAll(startTasks);

                var testMessage = new TestMessageRequest("Hello World", 123456);

                var requestHandlerTasks = new List<Task<bool>>();
                foreach (var client in clients) {
                    var reqTask = new TaskCompletionSource<bool>();
                    requestHandlerTasks.Add(reqTask.Task);
                    client.OnTypedBroadcastRequest += (s, e) => {
                        if (e.IsType(typeof(TestMessageRequest)) && e.TryDeserialize(out TestMessageRequest req)) {
                            Assert.AreEqual(testMessage.Text, req.Text, $"Request handler got invalid text on {client.Name}");
                            Assert.AreEqual(testMessage.Number, req.Number, $"Request handler got invalid number on {client.Name}");
                            var response = new TestMessageResponse($"My name is {client.Name}");
                            client.SendBroadcastResponse(response, e.Message);
                            reqTask.SetResult(true);
                        }
                        else {
                            reqTask.SetResult(false);
                        }
                    };
                }

                var results = await clients.First().SendBroadcastRequest<TestMessageResponse, TestMessageRequest>(testMessage, null, (currentRequest, totalRequests, sendingClientId, success, response) => {
                    if (currentRequest > 0) {
                        var client = clients.FirstOrDefault(c => c.Id == sendingClientId);
                        Assert.AreEqual($"My name is {client?.Name}", response.ResponseText, "Response text was different than expected");
                    }
                    else {
                        Assert.AreEqual(clients.Count, totalRequests, "TotalRequests was wrong during progress callback");
                    }
                }, TimeSpan.FromSeconds(5));

                Assert.AreEqual(clients.Count, results.Count, "Got unexpected number of results");

                foreach (var client in clients) {
                    client.Dispose();
                }
            }
            catch (Exception ex) { logger.Log(LogLevel.Error, ex.ToString()); Assert.Fail(ex.Message); }
        }

        [Test]
        public async Task Dummy_request_times_out() {
            using (var client = new UBNetClient("TestClient", ClientType.GameClient, new CancellationToken(), (a) => { a.Invoke(); }, _clientOptions, logger)) {
                await Helpers.StartClient(client, logger);
                var req = new DummyRequest();
                var task = client.SendRequest<PingResponse>(req, TimeSpan.FromSeconds(1));
                var delay = Task.Delay(TimeSpan.FromSeconds(5));
                await Task.WhenAny(delay, task);

                Assert.IsFalse(delay.IsCompleted, "Task did not time out");
                Assert.IsTrue(task.IsCompleted, "Task did not time out");
                Assert.IsNull(task.Result, "Task was not null");
            }
        }
    }
}
