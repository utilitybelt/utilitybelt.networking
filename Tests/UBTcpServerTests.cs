﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;

namespace UtilityBelt.Networking.Tests {
    [TestFixture]
    public class UBTcpServerTests {
        private Microsoft.Extensions.Logging.ILogger logger = new Extensions.Logging.NUnit.NUnitLogger("UBTcpServerTests", (message, level) => {
            return level >= LogLevel.Warning;
        });

        [Test]
        public void Can_instantiate_new_server_with_ip() {
            using (var server = new TCPListenServer("127.0.0.1", 21024, null)) {
                Assert.NotNull(server);
            }
        }

        [Test]
        public void Can_instantiate_new_server_with_allip() {
            using (var server = new TCPListenServer("0.0.0.0", 21024, null)) {
                Assert.NotNull(server);
            }
        }

        [Test]
        public async Task Can_start_and_stop_server() {
            var server = new TCPListenServer("127.0.0.1", 21024, null);

            await StartServer(server);
            Assert.IsTrue(server.IsListening);
            server.Stop();
            Assert.IsFalse(server.IsListening);
        }

        private async Task StartServer(TCPListenServer server) {
            _ = Task.Run(async () => {
                try {
                    await server.RunAsync();
                }
                catch (Exception e) {
                    logger.Log(LogLevel.Error, e.ToString());
                }
            });
            await Task.WhenAny(Task.Run(async () => { while (!server.IsListening) { await Task.Delay(1); } }), Task.Delay(1000));
        }
    }
}
