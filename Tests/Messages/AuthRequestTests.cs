﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Tests.Messages {
    [TestFixture]
    public class AuthRequestTests {
        [Test]
        public void Constructor_sets_properties() {
            var name = "Test 123";
            var clientType = ClientType.GameClient;
            var req = new AuthRequest(name, clientType);
            Assert.AreEqual(name, req.Name, "Name was not set correctly");
            Assert.AreEqual(clientType, req.ClientType, "ClientType was not set correctly");
        }
    }
}
