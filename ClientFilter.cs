﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityBelt.Networking.Lib;

namespace UtilityBelt.Networking {
    /// <summary>
    /// When specifying multiple checks (like client type and tags), this selects
    /// the behavior to use.
    /// </summary>
    public enum ClientFilterType {
        /// <summary>
        /// Make sure all rules that are passed to the filter are passed by each client.
        /// If you specify tags and type, this means they must *both* match the client.
        /// </summary>
        AND = 0,

        /// <summary>
        /// Check if *any* of the specified rules match the client. If you specify
        /// tags and client type, if either is matched the client is included.
        /// </summary>
        OR = 1
    }

    /// <summary>
    /// A client filter, used for sending messages to a filtered set of clients. If a rule is
    /// not specified (like not setting a tag list), the rule is ignored.
    /// </summary>
    [ProtoContract]
    public class ClientFilter {
        /// <summary>
        /// The type of logic to apply to this filter. AND means all specified rules must be matched
        /// by the client. OR means if any specified rule is matched the client will be included.
        /// </summary>
        [ProtoMember(1)]
        public ClientFilterType FilterType { get; } = ClientFilterType.AND;

        /// <summary>
        /// A list of client ids to filter by
        /// </summary>
        [ProtoMember(2)]
        public List<uint> Ids { get; } = new List<uint>();

        /// <summary>
        /// A list of client tags to filter by.  Specifying multiple tags means the client must
        /// have at least one of the tags (not all).
        /// </summary>
        [ProtoMember(3)]
        public List<string> Tags { get; } = new List<string>();

        /// <summary>
        /// The types of clients to filter by. Specifying multiple tags checks that the client
        /// is of one of the specified types.
        /// </summary>
        [ProtoMember(4)]
        public List<ClientType> ClientTypes { get; } = new List<ClientType>();

        /// <summary>
        /// Create a new client filter. Empty filters match all clients.
        /// </summary>
        public ClientFilter() {

        }

        /// <summary>
        /// Create a new client filter that matches a specific client id.
        /// </summary>
        /// <param name="id">The client id to match</param>
        public ClientFilter(uint id) {
            AddIds(new List<uint>() { id });
        }

        /// <summary>
        /// Create a new client filter that matches a specified tag.
        /// </summary>
        /// <param name="tag">That tag to match</param>
        public ClientFilter(string tag) {
            AddTags(new List<string>() { tag });
        }

        /// <summary>
        /// Create a new client filter that matches clients of the specified type.
        /// </summary>
        /// <param name="clientType">The client type to match</param>
        public ClientFilter(ClientType clientType) {
            AddClientTypes(new List<ClientType>() { clientType });
        }


        /// <summary>
        /// Create a new client filter that includes any clients with an id in the list.
        /// </summary>
        /// <param name="ids">A list of ids to match</param>
        public ClientFilter(IEnumerable<uint> ids) {
            AddIds(ids);
        }

        /// <summary>
        /// Create a new client filter that includes any clients with any of the specified tags.
        /// </summary>
        /// <param name="tags">The tags to match</param>
        public ClientFilter(IEnumerable<string> tags) {
            AddTags(tags);
        }

        /// <summary>
        /// Create a new client filter that includes any clients with any of the specified types.
        /// </summary>
        /// <param name="clientTypes">The client types to match</param>
        public ClientFilter(IEnumerable<ClientType> clientTypes) {
            AddClientTypes(clientTypes);
        }

        /// <summary>
        /// Create a new client filter and optionally specify ids, tags, client types, and logic type for multiple defined rules.
        /// Leaving any of these as null / empty will ignore that specific rule.
        /// </summary>
        /// <param name="ids">A list of ids to match against</param>
        /// <param name="tags">A list of tags to match against. Specifying multiple tags will check that the client includes *any* of these tags.</param>
        /// <param name="clientTypes">A list of client types to match against. Specifying multiple type will check that the client is a type contained in this list.</param>
        /// <param name="filterType">The type of logic to apply to this filter. AND means all specified rules must be matched
        /// by the client. OR means if any specified rule is matched the client will be included.</param>
        public ClientFilter(IEnumerable<uint> ids, IEnumerable<string> tags, IEnumerable<ClientType> clientTypes, ClientFilterType filterType) {
            FilterType = filterType;
            AddIds(ids);
            AddTags(tags);
            AddClientTypes(clientTypes);
        }

        private void AddIds(IEnumerable<uint> ids) {
            if (ids is null) return;
            foreach (var id in ids) {
                if (!Ids.Contains(id)) {
                    Ids.Add(id);
                }
            }
        }

        private void AddTags(IEnumerable<string> tags) {
            if (tags is null) return;
            foreach (var tag in tags) {
                if (!Tags.Contains(tag)) {
                    Tags.Add(tag);
                }
            }
        }

        private void AddClientTypes(IEnumerable<ClientType> clientTypes) {
            if (clientTypes is null) return;
            foreach (var clientType in clientTypes) {
                if (!ClientTypes.Contains(clientType)) {
                    ClientTypes.Add(clientType);
                }
            }
        }

        /// <summary>
        /// Check if this filter has any rules specified
        /// </summary>
        /// <returns></returns>
        public bool HasRules() {
            return Ids.Count > 0 || Tags.Count > 0 || ClientTypes.Count > 0;
        }

        /// <summary>
        /// Check if this filter matches the specified client
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public bool Matches(ITransportClient client) {
            if (!HasRules())
                return true;

            var hasTags = Tags.Count == 0 || Tags.Where(t => client.Tags.Contains(t)).Any();
            var hasClientTypes = ClientTypes.Count == 0 || ClientTypes.Contains(client.Type);
            var hasIds = Ids.Count == 0 || Ids.Contains(client.Id);

            if (FilterType == ClientFilterType.AND) {
                return hasTags && hasClientTypes && hasIds;
            }
            else {
                return hasTags || hasClientTypes || hasIds;
            }
        }
    }
}