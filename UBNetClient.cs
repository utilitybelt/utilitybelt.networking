﻿using Microsoft.Extensions.Logging;
using ProtoBuf;
using ProtoBuf.Meta;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;
using UtilityBelt.Networking.Lib.TCP;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking
{
    /// <summary>
    /// UBNet client
    /// </summary>
    public class UBNetClient : IDisposable {
        private ITransportClient _protoClient;
        private Action<Action> _runOnMainThread;
        private Dictionary<string, List<TypedChannelBroadcastHandler>> _channelSubscriptions = new Dictionary<string, List<TypedChannelBroadcastHandler>>();
        private Dictionary<string, List<CustomBroadcastHandler>> _typedBroadcastHandlers = new Dictionary<string, List<CustomBroadcastHandler>>();
        private Dictionary<string, Type> _customBroadcastTypes = new Dictionary<string, Type>();

        private class TypedChannelBroadcastHandler {
            public int HashCode { get; }
            public Action<string, uint, object> WrappedAction { get; }

            public TypedChannelBroadcastHandler(int hashcode, Action<string, uint, object> wrappedAction) {
                HashCode = hashcode;
                WrappedAction = wrappedAction;
            }
        }

        private class CustomBroadcastHandler {
            public int HashCode { get; }
            public Action<string, uint, object> WrappedAction { get; }

            public CustomBroadcastHandler(int hashcode, Action<string, uint, object> wrappedAction) {
                HashCode = hashcode;
                WrappedAction = wrappedAction;
            }
        }

        /// <summary>
        /// All UBNetClient instances
        /// </summary>
        public static List<UBNetClient> Instances { get; } = new List<UBNetClient>();
        private readonly ILogger _logger;
        private TimeSpan DefaultTimeout = TimeSpan.FromSeconds(5);

        /// <summary>
        /// Options
        /// </summary>
        public UBNetClientOptions Options { get; }

        /// <summary>
        /// True if connected to a ubnet server instance
        /// </summary>
        public bool IsConnected => _protoClient?.IsConnected == true;

        /// <summary>
        /// True if authed with the ubnet server
        /// </summary>
        public bool IsAuthed { get; private set; }

        /// <summary>
        /// True if we sent client info and got a response from the ubnet server
        /// </summary>
        public bool SentClientInfo { get; private set; }

        /// <summary>
        /// Client id of this ubnetclient.  this is set by the server
        /// </summary>
        public uint Id { get; private set; }

        /// <summary>
        /// The name of this client. Use SetName() to change.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of client this is.
        /// </summary>
        public ClientType Type { get; set; }

        /// <summary>
        /// Cancel this token to cancel all requests / connections.
        /// </summary>
        public CancellationToken Token { get; }
        
        /// <summary>
        /// The tags this client belongs to
        /// </summary>
        public List<string> Tags { get; } = new List<string>();

        /// <summary>
        /// Client properties
        /// </summary>
        public Dictionary<string, string> Properties { get; } = new Dictionary<string, string>();

        /// <summary>
        /// A list of remotely connected clients.
        /// </summary>
        public ConcurrentDictionary<uint, RemoteClient> Clients { get; } = new ConcurrentDictionary<uint, RemoteClient>();

        /// <summary>
        /// Fired when connected to the ubnet server
        /// </summary>
        public event EventHandler<EventArgs> OnConnected;

        /// <summary>
        /// Fired when disconnected from the ubnet server
        /// </summary>
        public event EventHandler<EventArgs> OnDisconnected;

        /// <summary>
        /// Fired when your subscribed channels have changed.
        /// </summary>
        public event EventHandler<EventArgs> OnChannelsChanged;

        /// <summary>
        /// Fired when we recieve a message from the server.
        /// </summary>
        public event EventHandler<MessageReceivedEventArgs> OnMessage;

        /// <summary>
        /// Fired when we recieve a message that requires a response.
        /// </summary>
        public event EventHandler<MessageReceivedEventArgs> OnRequestMessage;

        /// <summary>
        /// Fired when we recieve a message response
        /// </summary>
        public event EventHandler<MessageReceivedEventArgs> OnResponseMessage;

        /// <summary>
        /// Fired when a string message is recieved on a channel
        /// </summary>
        public event EventHandler<ChannelMessageReceivedEventArgs> OnChannelMessage;

        /// <summary>
        /// Fired when a typed message is recieved on a channel
        /// </summary>
        public event EventHandler<TypedChannelMessageReceivedEventArgs> OnTypedChannelMessage;

        /// <summary>
        /// Fired when a remote client connects to the ubnet server
        /// </summary>
        public event EventHandler<ClientEventArgs> OnRemoteClientConnected;

        /// <summary>
        /// Fired when a remote client's data (name/tags/etc) has been updated
        /// </summary>
        public event EventHandler<ClientEventArgs> OnRemoteClientUpdated;

        /// <summary>
        /// Fired when a remote client disconnects from ubnet
        /// </summary>
        public event EventHandler<ClientEventArgs> OnRemoteClientDisconnected;

        /// <summary>
        /// Fired when this client receives a typed broadcast message
        /// </summary>
        public event EventHandler<TypedBroadcastEventArgs> OnTypedBroadcastRequest;

        private Action<LogLevel, string> _log;

        private List<string> _channels = new List<string>();

        /// <summary>
        /// A list of channels this client is subscribed to.
        /// </summary>
        public IReadOnlyList<string> Channels => _channels;

        /// <summary>
        /// Create a new UBNet client
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <param name="runOnMainThread"></param>
        /// <param name="options"></param>
        /// <param name="logger"></param>
        public UBNetClient(string name, ClientType type, CancellationToken token, Action<Action> runOnMainThread, UBNetClientOptions options = null, ILogger logger = null) {
            Name = name;
            Type = type;
            Token = token;
            Options = options ?? new UBNetClientOptions();
            _logger = logger;

            _runOnMainThread = runOnMainThread;
            _log = (l, m) => {
                _runOnMainThread?.Invoke(() => {
                    logger?.Log(l, $"[UBNetClient:{Name}] {m}");
                });
            };
            _protoClient = new TCPClient(Options.Host, Options.Port, logger);

            Instances.Add(this);

            _protoClient.OnMessageReceived += _protoClient_OnMessageReceived;
            _protoClient.OnConnected += _protoClient_OnConnected;
            _protoClient.OnDisconnected += _protoClient_OnDisconnected;
        }

        #region Public API
        /// <summary>
        /// Set the name of this client.
        /// </summary>
        /// <param name="name">The name to use</param>
        /// <returns></returns>
        public async Task SetName(string name) {
            Name = name;
            await UpdateClientInfo();
        }

        /// <summary>
        /// Add the specified tags to this client.
        /// </summary>
        /// <param name="tags">A list of tags to add</param>
        /// <returns></returns>
        public async Task AddTags(IEnumerable<string> tags) {
            var didChange = false;
            foreach (var tag in tags) {
                if (!Tags.Contains(tag)) {
                    Tags.Add(tag);
                    didChange = true;
                }
            }

            if (didChange) {
                await UpdateClientInfo();
            }
        }

        /// <summary>
        /// Add a single tag to this client
        /// </summary>
        /// <param name="tag">The tag to add</param>
        /// <returns></returns>
        public async Task AddTag(string tag) {
            if (!Tags.Contains(tag)) {
                Tags.Add(tag);
                await UpdateClientInfo();
            }
        }

        /// <summary>
        /// Remove the specified tags from the client
        /// </summary>
        /// <param name="tags">A list of tags to remove</param>
        /// <returns></returns>
        public async Task RemoveTags(IEnumerable<string> tags) {
            var didChange = false;
            foreach (var tag in tags) {
                if (Tags.Contains(tag)) {
                    Tags.Remove(tag);
                    didChange = true;
                }
            }

            if (didChange) {
                await UpdateClientInfo();
            }
        }

        /// <summary>
        /// Remove the specified tag from the client
        /// </summary>
        /// <param name="tag">The tag to remove</param>
        /// <returns></returns>
        public async Task RemoveTag(string tag) {
            if (Tags.Contains(tag)) {
                Tags.Remove(tag);
                await UpdateClientInfo();
            }
        }

        /// <summary>
        /// Remove a property from this client with the specified key.
        /// </summary>
        /// <param name="property">The key of the property to remove</param>
        /// <returns></returns>
        public async Task RemoveProperty(string property) {
            if (Properties.ContainsKey(property)) {
                Properties.Remove(property);
                await UpdateClientInfo();
            }
        }

        /// <summary>
        /// Remove a set of properties from this client.
        /// </summary>
        /// <param name="properties">A list of property keys to remove</param>
        /// <returns></returns>
        public async Task RemoveProperties(IEnumerable<string> properties) {
            bool didChange = false;
            foreach (var property in properties) {
                if (Properties.ContainsKey(property)) {
                    Properties.Remove(property);
                    didChange = true;
                }
            }
            if (didChange) {
                await UpdateClientInfo();
            }
        }

        /// <summary>
        /// Set a client property
        /// </summary>
        /// <param name="property">The property key</param>
        /// <param name="value">The property value</param>
        /// <returns></returns>
        public async Task SetProperty(string property, string value) {
            if (Properties.ContainsKey(property)) {
                Properties[property] = value;
            }
            else {
                Properties.Add(property, value);
            }
            await UpdateClientInfo();
        }

        /// <summary>
        /// Set multiple client property key/value pairs
        /// </summary>
        /// <param name="properties">The properties to set</param>
        /// <returns></returns>
        public async Task SetProperties(Dictionary<string, string> properties) {
            foreach (var kv in properties) {
                if (Properties.ContainsKey(kv.Key)) {
                    Properties[kv.Key] = kv.Value;
                }
                else {
                    Properties.Add(kv.Key, kv.Value);
                }
            }
            await UpdateClientInfo();
        }

        /// <summary>
        /// Connect to the ubnet server and return. This awaits until a successful connection
        /// and auth has been made to the ubnet server.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Connect() {
            if (IsConnected)
                return true;

            try {
                var startTask = new TaskCompletionSource<bool>();
                EventHandler<EventArgs>? onConnected = null;
                onConnected = (s, e) => {
                    OnConnected -= onConnected;
                    startTask.SetResult(true);
                };
                OnConnected += onConnected;
                _ = RunAsync();

                await Task.WhenAny(startTask.Task, Task.Delay(10000));

                if (!startTask.Task.IsCompleted) {
                    _log(LogLevel.Error, $"Unable to Connect!");
                    return false;
                }
            }
            catch (Exception ex) { _log(LogLevel.Error, ex.ToString()); }

            return true;
        }

        /// <summary>
        /// Send the specified message object to the server. This object needs to be decorated with
        /// [ProtoContract] and [ProtoMember] attributes to serialize properly.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        public void Send<T>(T message) where T : IPacket {
            _protoClient.Send(message, Token);
        }

        /// <summary>
        /// Send the specified request object, expecting a response from the server.
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="message"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public async Task<TResponse> SendRequest<TResponse>(RequestPacket message, TimeSpan? timeout = null)
            where TResponse : ResponsePacket {

            var sendTask = _protoClient.SendAsync<TResponse>(message, Token);

            if (timeout is null) {
                timeout = DefaultTimeout;
            }

            await Task.WhenAny(sendTask, Task.Delay(timeout.Value));

            return sendTask.IsCompleted ? sendTask.Result : null;
        }

        /// <summary>
        /// Subscribe to the specified channel. You only get channel messages for channels
        /// you are subscribed to.
        /// </summary>
        /// <param name="channelName">The name of the channel to subscribe to</param>
        /// <returns></returns>
        public async Task SubscribeToChannel(string channelName) {
            var res = await SendRequest<ChannelSubscriptionResponse>(new ChannelSubscriptionRequest(channelName, true), DefaultTimeout);
            if (res != null) {
                _log(LogLevel.Debug, $"Got subscribe response: {String.Join(",", res.Channels)}");
            }
            else {
                _log(LogLevel.Debug, $"Got subscribe response: null res");
            }
        }

        /// <summary>
        /// Publish a typed message to a channel
        /// </summary>
        /// <param name="channel">The channel to publish to</param>
        /// <param name="message">The message to send. This needs to be decorated with the protobuf [ProtoContract] and [ProtoMember] attributes.</param>
        public void PublishToChannel<T>(string channel, T message) {
            if (message is string messageString) {
                Send(new ChannelBroadcast(channel, messageString));
            }
            else {
                var type = $"{message.GetType().Namespace}.{message.GetType().Name}";
                using (var stream = new MemoryStream()) {
                    Serializer.SerializeWithLengthPrefix(stream, message, PrefixStyle.Fixed32);
                    Send(new TypedChannelBroadcast(channel, type, stream.ToArray()));
                }
            }
        }

        /// <summary>
        /// Unsubscribe from the specified channel.
        /// </summary>
        /// <param name="channelName">The name of the channel to ubsubscribe from</param>
        /// <returns></returns>
        public async Task UnsubscribeFromChannel(string channelName) {
            await SendRequest<ChannelSubscriptionResponse>(new ChannelSubscriptionRequest(channelName, false), DefaultTimeout);
        }


        /// <summary>
        /// Send a broadcast packet to the specied clients, and return all the responses.
        /// </summary>
        /// <typeparam name="TResponse">The type of expected response</typeparam>
        /// <typeparam name="TRequest">The type of request</typeparam>
        /// <param name="message">The message to send</param>
        /// <param name="filter">A filter that chooses which clients to send to. Leaving this null will broadcast to all clients.</param>
        /// <param name="progressCallback"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public async Task<List<TResponse>> SendBroadcastRequest<TResponse, TRequest>(TRequest message, ClientFilter filter = null, Action<uint, uint, uint, bool, TResponse> progressCallback = null, TimeSpan? timeout = null){
            if (!timeout.HasValue) {
                timeout = DefaultTimeout;
            }

            var type = $"{message.GetType().Namespace}.{message.GetType().Name}";
            Task<List<TResponse>> sendTask;
            using (var stream = new MemoryStream()) {
                Serializer.SerializeWithLengthPrefix(stream, message, PrefixStyle.Fixed32);
                sendTask = _protoClient.SendAsyncBroadcast(new TypedBroadcastRequest(type, stream.ToArray(), filter), timeout.Value, progressCallback, Token);
            }

            if (!timeout.HasValue) {
                timeout = DefaultTimeout;
            }

            return await sendTask;
        }

        /// <summary>
        /// Send a response to a BroadcastRequest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response"></param>
        /// <param name="request"></param>
        public void SendBroadcastResponse<T>(T response, TypedBroadcastRequest request) {
            using (var stream = new MemoryStream()) {
                Serializer.SerializeWithLengthPrefix(stream, response, PrefixStyle.Fixed32);
                var data = stream.ToArray();
                Send(new TypedBroadcastResponse(Id, true, data, request));
            }
        }

        /// <summary>
        /// Stop / disconnect this client.
        /// </summary>
        public void Stop() {
            Instances.Remove(this);
            if (_protoClient != null) {
                _protoClient.OnConnected -= _protoClient_OnConnected;
                _protoClient.OnDisconnected -= _protoClient_OnDisconnected;
                _protoClient.OnMessageReceived -= _protoClient_OnMessageReceived;
                _protoClient?.Dispose();
            }
            _protoClient = null;
        }
        #endregion // Public API

        private async Task RunAsync() {
            await _protoClient.RunAsync(Token);
        }

        private async Task UpdateClientInfo() {
            if (SentClientInfo) {
                await SendClientInfo();
            }
        }

        private void _protoClient_OnDisconnected(object sender, ClientDisconnectedEventArgs e) {
            Id = 0;
            IsAuthed = false;
            SentClientInfo = false;
            Clients.Clear();
            _runOnMainThread?.Invoke(() => {
                OnDisconnected?.Invoke(this, EventArgs.Empty);
            });
            _log(LogLevel.Information, $"Disconnected from UBNet.");
        }

        private async void _protoClient_OnConnected(object sender, ClientConnectedEventArgs e) {
            try {
                var authResponse = await SendRequest<AuthResponse>(new AuthRequest(Name, Type));
                Id = authResponse.ClientId;
                IsAuthed = authResponse.ClientId > 0;

                await SendClientInfo();
                SentClientInfo = true;

                _runOnMainThread?.Invoke(() => {
                    OnConnected?.Invoke(this, EventArgs.Empty);
                });
                _log(LogLevel.Information, $"Connected to UBNet.");
            }
            catch { }
        }

        private async Task SendClientInfo() {
            await SendRequest<ClientInfoResponse>(new ClientInfoRequest(Id, Type, Name, Tags, Properties));
        }

        private async void _protoClient_OnMessageReceived(object sender, MessageReceivedEventArgs e) {
            _runOnMainThread?.Invoke(() => {
                OnMessage?.Invoke(this, e);
            });

            if (e.PacketHeader.Flags.HasFlag(PacketFlags.Request)) {
                _runOnMainThread?.Invoke(() => {
                    OnRequestMessage?.Invoke(this, e);
                });
            }
            else if (e.PacketHeader.Flags.HasFlag(PacketFlags.Response)) {
                _runOnMainThread?.Invoke(() => {
                    OnResponseMessage?.Invoke(this, e);
                });

                if (e.PacketHeader.Type == PacketType.ChannelSubscriptionResponse && e.Packet is ChannelSubscriptionResponse subResponse) {
                    _channels = subResponse.Channels;
                    _runOnMainThread?.Invoke(() => {
                        OnChannelsChanged?.Invoke(this, EventArgs.Empty);
                    });
                }
            }

            switch (e.PacketHeader.Type) {
                case PacketType.ChannelBroadcast:
                    if (e.Packet is ChannelBroadcast channelBroadcast) {
                        _runOnMainThread?.Invoke(() => {
                            OnChannelMessage?.Invoke(this, new ChannelMessageReceivedEventArgs(channelBroadcast));
                        });
                    }
                    break;
                case PacketType.TypedChannelBroadcast:
                    if (e.Packet is TypedChannelBroadcast typedChannelBroadcast) {
                        _runOnMainThread?.Invoke(() => {
                            OnTypedChannelMessage?.Invoke(this, new TypedChannelMessageReceivedEventArgs(typedChannelBroadcast));
                        });
                    }
                    break;
                case PacketType.ClientInfoUpdate:
                    if (e.Packet is ClientInfoUpdate clientUpdate) {
                        var client = Clients.Values.FirstOrDefault(c => c.Id == clientUpdate.ClientId);
                        if (client is null) {
                            var remote = new RemoteClient(clientUpdate.ClientId, clientUpdate.Type, clientUpdate.Name);
                            _log(LogLevel.Information, $"{clientUpdate.Name} (Client {clientUpdate.ClientId}/{clientUpdate.Type}) Connected.");
                            // todo: fix this
                            if (!Clients.TryAdd(remote.Id, remote)) {
                                _log(LogLevel.Error, $"Failed to add client {remote.Name} / {remote.Id}");
                            }
                            _runOnMainThread?.Invoke(() => {
                                OnRemoteClientConnected?.Invoke(this, new ClientEventArgs(remote, ClientEventType.Connected));
                            });
                            Clients.TryAdd(remote.Id, remote);
                        }
                        else {
                            client.Name = clientUpdate.Name;
                            client.Type = clientUpdate.Type;
                            client.Tags = clientUpdate.Tags;
                            client.Properties = clientUpdate.Properties;
                            _runOnMainThread?.Invoke(() => {
                                OnRemoteClientUpdated?.Invoke(this, new ClientEventArgs(client, ClientEventType.Updated));
                            });
                        }
                    }
                    break;
                case PacketType.ClientEvent:
                    if (e.Packet is ClientEvent clientEvent) {
                        switch(clientEvent.EventType) {
                            case ClientEventType.Connected:
                                if (Clients.ContainsKey(clientEvent.ClientId)) {
                                    break;
                                }
                                var remote = new RemoteClient(clientEvent.ClientId, clientEvent.ClientType, clientEvent.ClientName);
                                _log(LogLevel.Information, $"{clientEvent.ClientName} (Client {clientEvent.ClientId}/{clientEvent.ClientType}) Connected.");
                                // todo: fix this
                                if (!Clients.TryAdd(remote.Id, remote)) {
                                    _log(LogLevel.Error, $"Failed to add client {remote.Name} / {remote.Id}");
                                }
                                _runOnMainThread?.Invoke(() => {
                                    OnRemoteClientConnected?.Invoke(this, new ClientEventArgs(remote, ClientEventType.Connected));
                                });
                                break;
                            case ClientEventType.Disconnected:
                                var client = Clients.Values.FirstOrDefault(c => c.Id == clientEvent.ClientId);
                                if (client is not null) {
                                    _log(LogLevel.Information, $"{clientEvent.ClientName} (Client {clientEvent.ClientId}/{clientEvent.ClientType}) disconnected.");
                                    // todo: fix this
                                    if (!Clients.TryRemove(client.Id, out var _)) {
                                        _log(LogLevel.Error, $"Failed to remove client {client.Name} / {client.Id}");
                                    }
                                    _runOnMainThread?.Invoke(() => {
                                        OnRemoteClientDisconnected?.Invoke(this, new ClientEventArgs(client, ClientEventType.Disconnected));
                                    });
                                }
                                break;
                        }
                    }
                    break;
                case PacketType.TypedBroadcastRequest:
                    if (e.Packet is TypedBroadcastRequest typedBroadcast) {
                        _runOnMainThread?.Invoke(() => {
                            OnTypedBroadcastRequest?.Invoke(this, new TypedBroadcastEventArgs(typedBroadcast));
                        });
                    }
                    break;
            }
        }

        public void Dispose() {
            Stop();
        }
    }
}
