﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Lib.Packets;
using UtilityBelt.Networking.Lib.TCP;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking
{
    public class UBNetServer : IDisposable {
        private uint _nextClientId = 1;

        public CancellationTokenSource TokenSource { get; }

        private TCPListenServer _listenServer;
        private Action<LogLevel, string> _log;

        public UBNetServerOptions Options { get; }

        public bool IsListening => _listenServer?.IsListening == true;

        public event EventHandler<ClientConnectedEventArgs> OnClientConnected;
        public event EventHandler<ClientDisconnectedEventArgs> OnClientDisconnected;
        public event EventHandler<MessageReceivedEventArgs> OnClientMessage;

        public readonly ConcurrentDictionary<uint, ITransportClient> Clients = new ConcurrentDictionary<uint, ITransportClient>();

        public UBNetServer(UBNetServerOptions options = null, ILogger logger = null) {
            TokenSource = new CancellationTokenSource();
            Options = options ?? new UBNetServerOptions();
            _log = (l, m) => { logger?.Log(l, $"[UBNetServer] {m}"); };
            _listenServer = new TCPListenServer(options.BindIP, options.Port, logger);

            _listenServer.OnMessageReceived += _listenServer_OnMessageReceived;
            _listenServer.OnClientConnected += _listenServer_OnClientConnected;
            _listenServer.OnClientDisconnected += _listenServer_OnClientDisconnected;
        }

        private void Broadcast<T>(IEnumerable<ITransportClient> clients, T message) where T : IPacket {
            foreach (var client in clients) {
                client.Send(message, TokenSource.Token);
            }
        }

        private void _listenServer_OnClientConnected(object sender, ClientConnectedEventArgs e) {
            var clientId = _nextClientId++;
            e.Client.Id = clientId;
            Clients.TryAdd(e.Client.Id, e.Client);
        }

        private void _listenServer_OnClientDisconnected(object sender, ClientDisconnectedEventArgs e) {
            Clients.TryRemove(e.Client.Id, out var client);
            OnClientDisconnected?.Invoke(this, e);
            Broadcast(Clients.Values.Where(c => c.Id != e.Client.Id).ToList(), new ClientEvent(e.Client.Id, ClientEventType.Disconnected, e.Client.Name, e.Client.Type));
        }

        private async void _listenServer_OnMessageReceived(object sender, MessageReceivedEventArgs e) {
            _log(LogLevel.Trace, $"Message Received ({e.Client}): {e.PacketHeader.Type}");
            if (e.PacketHeader.Type == PacketType.AuthRequest && e.Packet is AuthRequest authRequest) {
                if (!e.Client.IsAuthed) {
                    e.Client.Name = authRequest.Name;
                    e.Client.Type = authRequest.ClientType;
                    e.Client.IsAuthed = true;
                    OnClientConnected?.Invoke(this, new ClientConnectedEventArgs(e.Client));
                    foreach (var client in Clients.Where(c => c.Key != e.Client.Id).Select(c => c.Value)) {
                        await e.Client.SendAsync(new ClientEvent(client.Id, ClientEventType.Connected, client.Name, client.Type), TokenSource.Token);
                        await e.Client.SendAsync(new ClientInfoUpdate(client.Id, client.Type, client.Name, client.Tags, client.Properties), TokenSource.Token);
                    }
                    Broadcast(Clients.Values.Where(c => c.Id != e.Client.Id).ToList(), new ClientEvent(e.Client.Id, ClientEventType.Connected, e.Client.Name, e.Client.Type));
                }
                await e.Client.SendAsync(new AuthResponse(e.Client.Id, authRequest), TokenSource.Token);
            }

            if (!e.Client.IsAuthed) {
                _log(LogLevel.Warning, $"Client {e.Client} tried to send {e.PacketHeader.Type} before authed");
                return;
            }

            if (e.PacketHeader.Type == PacketType.PingRequest && e.Packet is PingRequest pingRequest) {
                e.Client.Send(new PingResponse(pingRequest), TokenSource.Token);
            }
            else if (e.PacketHeader.Type == PacketType.ChannelSubscriptionRequest && e.Packet is ChannelSubscriptionRequest subRequest) {
                if (subRequest.Subscribe) {
                    if (!e.Client._channels.Contains(subRequest.Channel)) {
                        e.Client._channels.Add(subRequest.Channel);
                    }
                }
                else {
                    e.Client._channels.Remove(subRequest.Channel);
                }
                e.Client.Send(new ChannelSubscriptionResponse(e.Client._channels, subRequest), TokenSource.Token);
            }
            else if (e.PacketHeader.Type == PacketType.ClientInfoRequest && e.Packet is ClientInfoRequest cInfoRequest) {
                if (e.Client.Id != cInfoRequest.ClientId) {
                    _log(LogLevel.Warning, $"Client {e.Client} tried to send ClientInfoRequest with id {cInfoRequest.ClientId}");
                    cInfoRequest.ClientId = e.Client.Id;
                }
                e.Client.Name = cInfoRequest.Name;
                e.Client.Tags = cInfoRequest.Tags;
                e.Client.Type = cInfoRequest.Type;
                e.Client.Properties = cInfoRequest.Properties;
                e.Client.Send(new ClientInfoResponse(cInfoRequest), TokenSource.Token);
                var clientInfoUpdate = new ClientInfoUpdate(e.Client.Id, cInfoRequest.Type, cInfoRequest.Name, cInfoRequest.Tags, cInfoRequest.Properties);
                foreach (var client in Clients.Values.Where(c => c.Id != e.Client.Id).ToList()) {
                    client.Send(clientInfoUpdate, TokenSource.Token);
                }
            }
            else if (e.PacketHeader.Type == PacketType.TypedBroadcastRequest && e.Packet is TypedBroadcastRequest typedBroadcastRequest) {
                DoTypedBroadcastRequest(e, typedBroadcastRequest);
            }
            else if (e.PacketHeader.Type == PacketType.ChannelBroadcast && e.Packet is ChannelBroadcast broadcast) {
                broadcast.SendingClientId = e.Client.Id;
                foreach (var client in Clients.Values.Where(c => c.Channels.Contains(broadcast.Channel)).ToList()) {
                    client.Send(broadcast, TokenSource.Token);
                }
            }
            else if (e.PacketHeader.Flags.HasFlag(PacketFlags.Broadcast) && e.Packet is BroadcastPacket broadcastPacket) {
                broadcastPacket.SendingClientId = e.Client.Id;
                foreach (var client in Clients.Values.Where(broadcastPacket.ClientFilter.Matches).ToList()) {
                    client.Send(broadcastPacket, TokenSource.Token);
                }
            }

            OnClientMessage?.Invoke(this, e);
        }

        private async void DoTypedBroadcastRequest(MessageReceivedEventArgs e, TypedBroadcastRequest typedBroadcastRequest) {
            var clients = Clients.Values.Where(typedBroadcastRequest.ClientFilter.Matches);
            e.Client.Send(new TypedBroadcastResult(0, (uint)clients.Count(), 0, default, typedBroadcastRequest), TokenSource.Token);

            var tasks = new List<Task<TypedBroadcastResponse>>();
            int total = 0;
            foreach (var client in clients) {
                _ = Task.Run(async () => {
                    var response = await client.SendAsync<TypedBroadcastResponse>(new TypedBroadcastRequest(typedBroadcastRequest.Type, typedBroadcastRequest.Data), TokenSource.Token);
                    response.ClientId = client.Id;
                    var currentTotal = Interlocked.Add(ref total, 1);
                    e.Client.Send(new TypedBroadcastResult(client.Id, (uint)clients.Count(), (uint)currentTotal, response, typedBroadcastRequest), TokenSource.Token);
                });
            }
        }

        public async Task<bool> Start() {
            try {
                _ = Task.Run(async () => {
                    try {
                        await RunAsync();
                    }
                    catch (Exception ex) { _log(LogLevel.Error, ex.ToString()); }
                });
                await Task.WhenAny(Task.Run(async () => { while (!IsListening) { await Task.Delay(1); } }), Task.Delay(5000));
            }
            catch (Exception ex) { _log(LogLevel.Error, ex.ToString()); }

            return IsListening;
        }

        private async Task RunAsync() {
            _log(LogLevel.Information, $"Starting TCP server on: {Options.BindIP}:{Options.Port}");
            await _listenServer.RunAsync();
        }

        public void Stop() {
            _log(LogLevel.Information, $"Stopping TCP server");
            _listenServer?.Stop();
        }

        public void Dispose() {
            if (_listenServer is not null) {
                _listenServer.OnMessageReceived -= _listenServer_OnMessageReceived;
                _listenServer.Dispose();
            }
        }
    }
}
