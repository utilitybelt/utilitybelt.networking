﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Networking {
    /// <summary>
    /// Options for creating a new ubnet server instance
    /// </summary>
    public class UBNetServerOptions {

        /// <summary>
        /// The ip of the interface to bind to. Use 0.0.0.0 to bind to all available interfaces.
        /// </summary>
        public string BindIP { get; set; } = "127.0.0.1";

        /// <summary>
        /// The port to listen on.
        /// </summary>
        public int Port { get; set; } = 42163;
        public UBNetServerOptions(string bindIP, int port) {
            BindIP = bindIP;
            Port = port;
        }

        public UBNetServerOptions() { }
    }
}
