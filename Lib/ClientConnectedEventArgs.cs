﻿using System;
using UtilityBelt.Networking.Lib.TCP;

namespace UtilityBelt.Networking.Lib {
    public class ClientConnectedEventArgs : EventArgs {
        public ITransportClient Client { get; }

        public ClientConnectedEventArgs(ITransportClient client) {
            Client = client;
        }
    }
}