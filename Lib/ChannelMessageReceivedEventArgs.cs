﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib {
    /// <summary>
    /// Event args for when a string channel message is received.
    /// </summary>
    public class ChannelMessageReceivedEventArgs : EventArgs {

        /// <summary>
        /// The id of the client sending the message
        /// </summary>
        public uint SendingClientId { get; }

        /// <summary>
        /// The broadcast
        /// </summary>
        public ChannelBroadcast ChannelBroadcast { get; }

        internal ChannelMessageReceivedEventArgs(ChannelBroadcast channelBroadcast) {
            ChannelBroadcast = channelBroadcast;
            SendingClientId = channelBroadcast.SendingClientId;
        }
    }
}
