﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Networking.Lib {
    /// <summary>
    /// Client types
    /// </summary>
    public enum ClientType : ushort {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Game clients
        /// </summary>
        GameClient = 1,

        /// <summary>
        /// Game servers
        /// </summary>
        GameServer = 2,

        /// <summary>
        /// Game Launchers
        /// </summary>
        Launcher = 3,

        /// <summary>
        /// Others
        /// </summary>
        Other = 999
    }
}
