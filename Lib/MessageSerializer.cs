﻿using Microsoft.Extensions.Logging;
using ProtoBuf;
using ProtoBuf.Meta;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib
{
    internal static class MessageSerializer {
        private static int _nextTypeId = 7000;

        public static byte[] Serialize<T>(uint senderId, T packet) {
            PacketType type = PacketType.None;
            if (packet is IPacket p) {
                type = p.PacketType;
            }
            var header = new PacketHeader() {
                Type = type,
                SendingClientId = senderId
            };

            if (packet is ResponsePacket) {
                header.Flags |= PacketFlags.Response;
            }

            if (packet is RequestPacket) {
                header.Flags |= PacketFlags.Request;
            }

            if (packet is BroadcastPacket) {
                header.Flags |= PacketFlags.Broadcast;
            }

            byte[] headerBytes;
            byte[] bodyBytes;
            using (var stream = new MemoryStream()) {
                Serializer.SerializeWithLengthPrefix(stream, header, PrefixStyle.Fixed32);
                headerBytes = stream.ToArray();
            }
            using (var stream = new MemoryStream()) {
                Serializer.SerializeWithLengthPrefix(stream, packet, PrefixStyle.Fixed32);
                bodyBytes = stream.ToArray();
            }
            using (var stream = new MemoryStream()) {
                using (var writer = new BinaryWriter(stream)) {
                    writer.Write(headerBytes);
                    writer.Write(bodyBytes);
                }
                return stream.ToArray();
            }
        }

        public static bool TryReadMessage(byte[] data, out PacketHeader header, out IPacket packet, Action<LogLevel, string> _log = null) {
            using (var stream = new MemoryStream(data))
            using (var reader = new BinaryReader(stream)) {
                header = Serializer.DeserializeWithLengthPrefix<PacketHeader>(stream, PrefixStyle.Fixed32);
                
                switch (header.Type) {
                    case PacketType.PingRequest:
                        packet = DeserializeOrMerge<PingRequest>(stream);
                        break;
                    case PacketType.PingResponse:
                        packet = DeserializeOrMerge<PingResponse>(stream);
                        break;
                    case PacketType.DummyRequest:
                        packet = DeserializeOrMerge<DummyRequest>(stream);
                        break;
                    case PacketType.AuthRequest:
                        packet = DeserializeOrMerge<AuthRequest>(stream);
                        break;
                    case PacketType.AuthResponse:
                        packet = DeserializeOrMerge<AuthResponse>(stream);
                        break;
                    case PacketType.ChannelBroadcast:
                        packet = DeserializeOrMerge<ChannelBroadcast>(stream);
                        break;
                    case PacketType.ChannelSubscriptionRequest:
                        packet = DeserializeOrMerge<ChannelSubscriptionRequest>(stream);
                        break;
                    case PacketType.ChannelSubscriptionResponse:
                        packet = DeserializeOrMerge<ChannelSubscriptionResponse>(stream);
                        break;
                    case PacketType.ClientInfoRequest:
                        packet = DeserializeOrMerge<ClientInfoRequest>(stream);
                        break;
                    case PacketType.ClientInfoResponse:
                        packet = DeserializeOrMerge<ClientInfoResponse>(stream);
                        break;
                    case PacketType.ClientInfoUpdate:
                        packet = DeserializeOrMerge<ClientInfoUpdate>(stream);
                        break;
                    case PacketType.ClientEvent:
                        packet = DeserializeOrMerge<ClientEvent>(stream);
                        break;
                    case PacketType.TypedBroadcastRequest:
                        packet = DeserializeOrMerge<TypedBroadcastRequest>(stream);
                        break;
                    case PacketType.TypedBroadcastResponse:
                        packet = DeserializeOrMerge<TypedBroadcastResponse>(stream);
                        break;
                    case PacketType.TypedBroadcastResult:
                        packet = DeserializeOrMerge<TypedBroadcastResult>(stream);
                        break;
                    case PacketType.TypedChannelBroadcast:
                        packet = DeserializeOrMerge<TypedChannelBroadcast>(stream);
                        break;
                    default:
                        packet = null;
                        return false;
                }
            }

            return true;
        }

        public static string ToString(object packet) {
            if (packet is null) {
                return "null";
            }

            var propParts = new List<string>();
            foreach (var prop in packet.GetType().GetProperties()) {
                propParts.Add($"{prop.Name}={FormatValue(prop.GetValue(packet))}");
            }

            return $"{packet.GetType().Name}<{string.Join(",", propParts)}>";
        }

        private static object FormatValue(object v) {
            if (v is null)
                return "null";

            if (v.GetType() != typeof(string) && v is IEnumerable en) {
                List<string> values = new List<string>();
                foreach (var i in en) {
                    values.Add(i.ToString());
                }
                return $"[{string.Join(",", values)}]";
            }

            return v.ToString();
        }

        public static T DeserializeOrMerge<T>(Stream stream) {
            if (!typeof(T).IsValueType
                && typeof(T) != typeof(string)
                // Test to make sure T has a public default constructor
                && typeof(T).GetConstructor(Type.EmptyTypes) != null
                && typeof(T).HasProtoIncludeAtributes()) {
                return ProtoBuf.Serializer.MergeWithLengthPrefix(stream, Activator.CreateInstance<T>(), PrefixStyle.Fixed32);
            }
            else {
                return ProtoBuf.Serializer.DeserializeWithLengthPrefix<T>(stream, PrefixStyle.Fixed32);
            }
        }

        public static bool HasProtoIncludeAtributes(this Type type) {
            if (type == null)
                throw new ArgumentNullException();
            if (!type.IsDefined(typeof(ProtoContractAttribute), false))
                return false;
            return type.BaseTypesAndSelf().SelectMany(t => t.GetCustomAttributes(false)).Where(t => t is ProtoIncludeAttribute).Any();
        }

        public static IEnumerable<Type> BaseTypesAndSelf(this Type type) {
            while (type != null) {
                yield return type;
                type = type.BaseType;
            }
        }

        public static void RegisterRequestType(Type type) {
            if (!RuntimeTypeModel.Default.CanSerialize(type)) {
                RuntimeTypeModel.Default.Add(typeof(RequestPacket), false).AddSubType(_nextTypeId++, type);
            }
        }

        public static void RegisterResponseType(Type type) {
            if (!RuntimeTypeModel.Default.CanSerialize(type)) {
                RuntimeTypeModel.Default.Add(typeof(ResponsePacket), false).AddSubType(_nextTypeId++, type);
            }
        }
    }
}
