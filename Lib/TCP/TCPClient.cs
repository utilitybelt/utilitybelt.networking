﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unclassified.Util;
using Microsoft.Extensions.Logging;
using ProtoBuf;
using UtilityBelt.Networking.Messages;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Lib.TCP {
    public class TCPClient : ITransportClient {
        private TcpClient _client;
        private NetworkStream _stream;
        private TaskCompletionSource<bool> closedTcs = new TaskCompletionSource<bool>();

        private ILogger _logger;
        private PacketProtocol _proto;
        private Action<LogLevel, string> _log;

        private TcpClient _serverClient { get; set; }

        public override event EventHandler<ClientConnectedEventArgs> OnConnected;
        public override event EventHandler<ClientDisconnectedEventArgs> OnDisconnected;
        public override event EventHandler<MessageReceivedEventArgs> OnMessageReceived;

        /// <summary>
        /// Gets a value indicating whether the client is currently connected.
        /// </summary>
        public override bool IsConnected => _client?.Client?.Connected == true;

        private ByteBuffer ByteBuffer { get; set; } = new ByteBuffer();

        public Task ClosedTask => closedTcs.Task;

        internal TCPClient(TcpClient client, ILogger logger = null) : base() {
            _serverClient = client;
            closedTcs.SetResult(true);
            _logger = logger;
            SetupPacketProtocol();
        }

        public TCPClient(string host, int port, ILogger logger = null) : base(host, port) {
            closedTcs.SetResult(true);
            _logger = logger;
            SetupPacketProtocol();
        }

        private void SetupPacketProtocol() {
            _log = (l, m) => { _logger?.Log(l, $"[UBTCPClient:{(_serverClient == null ? "Local" : "Server")}] {m}"); };
            _proto = new PacketProtocol(1024 * 1024 * 10, _logger);
            _proto.MessageArrived = (data) => {
                try {
                    if (MessageSerializer.TryReadMessage(data, out PacketHeader packetHeader, out IPacket packet, _log)) {
                        _log(LogLevel.Trace, $"Read Message:\n\t- {packetHeader}\n\t- {packet}");
                        OnMessageReceived?.Invoke(this, new MessageReceivedEventArgs(this, packetHeader, packet));
                    }
                    else {
                        _log(LogLevel.Warning, $"Read Invalid Message:\n\t- {packetHeader}\n\t- {packet}");
                    }
                }
                catch (Exception ex) { _log(LogLevel.Error, ex.ToString()); }
            };
        }

        public override async Task RunAsync(CancellationToken cancellationToken = default) {
            var initialCount = 0;
            bool isReconnected = false;
            int reconnectTry = -1;
            do {
                reconnectTry++;
                ByteBuffer = new ByteBuffer();
                if (_serverClient != null) {
                    // Take accepted connection from listener
                    _client = _serverClient;
                }
                else {
                    // Try to connect to remote host
                    var connectTimeout = TimeSpan.FromTicks(ConnectTimeout.Ticks + (MaxConnectTimeout.Ticks - ConnectTimeout.Ticks) / 20 * Math.Min(reconnectTry, 20));
                    _client = new TcpClient(AddressFamily.InterNetworkV6);
                    _client.Client.DualMode = true;
                    _log(LogLevel.Information, "Connecting to server");
                    Task connectTask = _client.ConnectAsync(Host, Port);
                    var timeoutTask = Task.Delay(connectTimeout);
                    if (await Task.WhenAny(connectTask, timeoutTask) == timeoutTask) {
                        _log(LogLevel.Information, $"Connection timeout ({connectTimeout.TotalSeconds:N0} seconds)");
                        continue;
                    }
                    try {
                        await connectTask;
                    }
                    catch (Exception ex) {
                        _log(LogLevel.Error, $"Error connecting to remote host: {ex.Message}");
                        await timeoutTask;
                        continue;
                    }
                }
                reconnectTry = -1;
                _stream = _client.GetStream();

                // Read until the connection is closed.
                // A closed connection can only be detected while reading, so we need to read
                // permanently, not only when we might use received data.
                var networkReadTask = Task.Run(async () => {
                    // 10 KiB should be enough for every Ethernet packet
                    byte[] buffer = new byte[10240];
                    while (!cancellationToken.IsCancellationRequested) {
                        int readLength = 0;
                        try {
                            readLength = await _stream?.ReadAsync(buffer, 0, buffer.Length);
                        }
                        catch (IOException ex) when ((ex.InnerException as SocketException)?.ErrorCode == (int)SocketError.OperationAborted ||
                            (ex.InnerException as SocketException)?.ErrorCode == 125 /* Operation canceled (Linux) */) {
                            // Warning: This error code number (995) may change.
                            // See https://docs.microsoft.com/en-us/windows/desktop/winsock/windows-sockets-error-codes-2
                            // Note: NativeErrorCode and ErrorCode 125 observed on Linux.
                            _log(LogLevel.Debug, $"Connection closed locally: {ex.Message}");
                            readLength = -1;
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.ClosedLocally));
                        }
                        catch (IOException ex) when ((ex.InnerException as SocketException)?.ErrorCode == (int)SocketError.ConnectionAborted) {
                            _log(LogLevel.Debug, $"Connection aborted: {ex.Message}");
                            readLength = -1;
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.Aborted));
                        }
                        catch (IOException ex) when ((ex.InnerException as SocketException)?.ErrorCode == (int)SocketError.ConnectionReset) {
                            _log(LogLevel.Debug, $"Connection reset remotely: {ex.Message}");
                            readLength = -2;
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.CloseRemotely));
                        }
                        catch (TaskCanceledException ex) {
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.ClosedLocally));
                            _log(LogLevel.Debug, $"Disconnected: {ex.Message}");
                            return;
                        }
                        catch (ObjectDisposedException ex) {
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.ClosedLocally));
                            _log(LogLevel.Debug, $"Connection closed: {ex.Message}");
                        }
                        catch (SocketException ex) {
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.CloseRemotely));
                            _log(LogLevel.Debug, $"Connection closed: {ex.Message}");
                        }
                        catch (Exception ex) {
                            OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.ClosedLocally));
                            _log(LogLevel.Debug, $"Disconnected: {ex.Message}");
                            closedTcs.TrySetResult(true);
                            return;
                        }
                        if (readLength <= 0) {
                            if (readLength == 0) {
                                _log(LogLevel.Debug, $"Connection closed remotely");
                                OnDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(this, DisconnectReason.CloseRemotely));
                            }
                            closedTcs.TrySetResult(true);
                            return;
                        }
                        var segment = new ArraySegment<byte>(buffer, 0, readLength);
                        ByteBuffer.Enqueue(segment);
                        await OnReceivedAsync(readLength);
                    }
                });

                closedTcs = new TaskCompletionSource<bool>();
                await OnConnectedAsync(isReconnected);

                // Wait for closed connection
                await networkReadTask;
                _client.Close();

                isReconnected = true;
            }
            while (AutoReconnect && _serverClient == null);
        }

        public override void Stop() {
            _client?.Client?.Disconnect(false);
        }

        public override async void Send<T>(T message, CancellationToken cancellationToken = default) {
            _log(LogLevel.Trace, $"Send Message to {Name}: {message}");
            await WriteMessage(message);
        }

        public override async Task SendAsync<T>(T message, CancellationToken cancellationToken = default) {
            _log(LogLevel.Trace, $"Send Message to {Name}: {message}");
            await WriteMessage(message);
        }

        private async Task WriteMessage<T>(T message) where T : IPacket {
            if (_client?.Client?.Connected != true)
                return;
            var messageBytes = MessageSerializer.Serialize(Id, message);
            var packetBytes = PacketProtocol.WrapMessage(messageBytes);
            try {
                await _stream.WriteAsync(packetBytes, 0, packetBytes.Length);
            }
            catch (Exception ex) { _log(LogLevel.Debug, $"Failed to write {message} // {(packetBytes is null ? "null" : packetBytes)} // {(_stream is null ? "null" : _stream)} // {ex}"); }
        }

        public override async Task<TResponse> SendAsync<TResponse>(RequestPacket message, CancellationToken cancellationToken = default) {
            var t = new TaskCompletionSource<TResponse>();
            EventHandler<MessageReceivedEventArgs> onMessageReceived = (s, e) => {
                try {
                    if (e.PacketHeader.Flags.HasFlag(PacketFlags.Response) && e.Packet is TResponse response) {
                        if (response.RequestPacketId == message.PacketId && Id == e.Client.Id) {
                            t.TrySetResult(response);
                        }
                    }
                }
                catch (Exception ex) { _log(LogLevel.Error, ex.ToString()); }
            };
            OnMessageReceived += onMessageReceived;
            try {
                // TODO: this probably shouldnt be here...?
                message.PacketId = Interlocked.Add(ref RequestPacket._nextPacketId, 1);
                await WriteMessage(message);
                await Task.WhenAny(t.Task, Task.Delay(TimeSpan.FromSeconds(5)));
            }
            finally {
                OnMessageReceived -= onMessageReceived;
            }

            return t.Task.Result;
        }

        /// <summary>
        /// Send a broadcast to multiple clients. This expects a response from all clients.
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="message"></param>
        /// <param name="progressCallback"></param>
        /// <param name="timeout"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override async Task<List<TResponse>> SendAsyncBroadcast<TResponse>(RequestPacket message, TimeSpan timeout, Action<uint, uint, uint, bool, TResponse> progressCallback, CancellationToken cancellationToken) {
            var t = new TaskCompletionSource<bool>();
            var responses = new List<TResponse>();
            var currentTotal = Interlocked.Add(ref RequestPacket._nextPacketId, 1);
            EventHandler<MessageReceivedEventArgs> onMessageReceived = (s, e) => {
                try {
                    if (e.Packet is TypedBroadcastResult result && result.RequestPacketId == message.PacketId) {
                        if (result.CurrentRequest > 0) {
                            TResponse responseObj;
                            try {
                                using (var stream = new MemoryStream(result.Response.Data)) {
                                    responseObj = Serializer.DeserializeWithLengthPrefix<TResponse>(stream, PrefixStyle.Fixed32);
                                }
                            }
                            catch (Exception ex) {
                                _log(LogLevel.Error, ex.ToString());
                                responseObj = default;
                            }
                            responses.Add(responseObj);
                            progressCallback?.Invoke(result.CurrentRequest, result.TotalRequests, result.ResponseClientId, result.Response.Success, responseObj);
                        }
                        else {
                            progressCallback?.Invoke(result.CurrentRequest, result.TotalRequests, 0, true, default);
                        }

                        if (responses.Count == result.TotalRequests) {
                            t.SetResult(true);
                        }
                    }
                }
                catch (Exception ex) { _log(LogLevel.Error, ex.ToString()); }
            };
            OnMessageReceived += onMessageReceived;
            try {
                Send(message, cancellationToken);
                await Task.WhenAny(t.Task, Task.Delay(timeout));
            }
            finally {
                OnMessageReceived -= onMessageReceived;
            }

            return responses;
        }

        protected Task OnConnectedAsync(bool isReconnected) {
            OnConnected?.Invoke(this, new ClientConnectedEventArgs(this));
            return Task.CompletedTask;
        }

        protected async Task OnReceivedAsync(int count) {
            var bytes = await ByteBuffer.DequeueAsync(count);

            _proto.DataReceived(bytes);

            return;
        }

        public override void Dispose() {
            AutoReconnect = false;
            _client?.Dispose();
            _stream = null;
        }
    }
}
