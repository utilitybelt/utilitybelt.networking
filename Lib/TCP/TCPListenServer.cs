﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib.TCP;

namespace UtilityBelt.Networking.Lib {
    public class TCPListenServer : IListenServer {
        private TcpListener tcpListener;
        private CancellationTokenSource _tokenSource;
        private bool _listening;
        private CancellationToken _token;
        private ILogger _logger;
        private Action<LogLevel, string> _log;

        private Dictionary<TcpClient, ITransportClient> _clients = new Dictionary<TcpClient, ITransportClient>();

        public override event EventHandler<ClientConnectedEventArgs> OnClientConnected;
        public override event EventHandler<ClientDisconnectedEventArgs> OnClientDisconnected;
        public override event EventHandler<MessageReceivedEventArgs> OnMessageReceived;

        public override bool IsListening => _listening;
        public IPAddress ListenAddress { get; private set; }

        public TCPListenServer(string host, int port, ILogger logger) : base(host, port) {
            _logger = logger;
            _log = (l, m) => { logger?.Log(l, $"[UBTCPListenServer] {m}"); };
            if (string.IsNullOrEmpty(host) || host == "0.0.0.0") {
                ListenAddress = IPAddress.Any;
            }
            else if (IPAddress.TryParse(host, out IPAddress address)) {
                ListenAddress = address.MapToIPv4();
            }
            else {
                throw new Exception($"Unable to parse host to ip: {host}");
            }
        }

        /// <summary>
		/// Starts listening asynchronously for incoming connection requests.
		/// </summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		public override async Task RunAsync(CancellationToken? token = null) {
            if (tcpListener != null)
                throw new InvalidOperationException("The listener is already running.");
            _log(LogLevel.Information, $"Starting listener on {Host}:{Port}");

            _tokenSource = CancellationTokenSource.CreateLinkedTokenSource(token ?? new CancellationToken());
            _token = _tokenSource.Token;

            tcpListener = new TcpListener(ListenAddress, Port);
            tcpListener.Start();

            var clients = new ConcurrentDictionary<TcpClient, bool>();   // bool is dummy, never regarded
            var clientTasks = new List<Task>();
            try {
                _listening = true;
                while (!_token.IsCancellationRequested && _listening && tcpListener != null) {
                    _token.ThrowIfCancellationRequested();

                    TcpClient tcpClient;
                    try {
                        tcpClient = await tcpListener?.AcceptTcpClientAsync();
                    }
                    catch (SocketException) when (_token.IsCancellationRequested || !_listening || tcpListener is null) {
                        break;
                    }
                    var endpoint = tcpClient.Client.RemoteEndPoint;
                    _log(LogLevel.Debug, "Client connected from " + endpoint);
                    clients.TryAdd(tcpClient, true);
                    var clientTask = Task.Run(async () => {
                        await HandleClient(tcpClient);
                        if (_clients.TryGetValue(tcpClient, out var client)) {
                            _log(LogLevel.Debug, "Client disconnected from " + endpoint);
                            OnClientDisconnected?.Invoke(this, new ClientDisconnectedEventArgs(client, DisconnectReason.ClosedLocally));
                            _clients.Remove(tcpClient);
                        }
                        tcpClient.Dispose();
                        clients.TryRemove(tcpClient, out _);
                    });
                    clientTasks.Add(clientTask);
                }
            }
            finally {
                _listening = false;
                foreach (var tcpClient in clients.Keys) {
                    tcpClient.Dispose();
                }
                await Task.WhenAll(clientTasks);
                clientTasks.Clear();
                tcpListener = null;
            }
        }

        private async Task HandleClient(TcpClient tcpClient) {
            var client = new TCPClient(tcpClient, _logger);
            _clients.Add(tcpClient, client);
            OnClientConnected?.Invoke(this, new ClientConnectedEventArgs(client));
            client.OnMessageReceived += Client_OnMessageReceived;
            await client.RunAsync(_tokenSource.Token);
            client.OnMessageReceived -= Client_OnMessageReceived;
        }

        private void Client_OnMessageReceived(object sender, MessageReceivedEventArgs e) {
            OnMessageReceived?.Invoke(sender, e);
        }

        public override void Stop() {
            _listening = false;
            _tokenSource?.Cancel();
            tcpListener?.Stop();
            tcpListener = null;
        }

        public override void Dispose() {
            Stop();
        }
    }
}
