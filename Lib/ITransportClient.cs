﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Lib
{
    public abstract class ITransportClient : IDisposable {
        public string Host { get; }
        public int Port { get; }

        public abstract bool IsConnected { get; }

        public abstract event EventHandler<ClientConnectedEventArgs> OnConnected;
        public abstract event EventHandler<ClientDisconnectedEventArgs> OnDisconnected;
        public abstract event EventHandler<MessageReceivedEventArgs> OnMessageReceived;

        public uint Id { get; internal set; }
        public bool IsAuthed { get; internal set; }

        /// <summary>
        /// The friendly name of the client
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// The type of client this is
        /// </summary>
        public ClientType Type { get; internal set; }

        /// <summary>
        /// The tags this client belongs to.
        /// </summary>
        public List<string> Tags { get; internal set; } = new List<string>();

        /// <summary>
        /// A list of string key/value pairs. you can store custom data here.
        /// </summary>
        public Dictionary<string, string> Properties { get; internal set; } = new Dictionary<string, string>();


        public TimeSpan ConnectTimeout { get; set; } = TimeSpan.FromSeconds(5);

        public TimeSpan MaxConnectTimeout { get; set; } = TimeSpan.FromMinutes(1);

        public bool AutoReconnect { get; set; } = true;

        internal List<string> _channels = new List<string>();
        public IReadOnlyList<string> Channels => _channels;

        internal ITransportClient() {
            Host = "internal";
        }

        public ITransportClient(string host, int port) {
            Host = host;
            Port = port;
        }

        public abstract Task RunAsync(CancellationToken cancellationToken);

        public abstract void Stop();

        public abstract void Send<T>(T message, CancellationToken cancellationToken) where T : IPacket;
        public abstract Task SendAsync<T>(T message, CancellationToken cancellationToken) where T : IPacket;

        public abstract Task<TResponse> SendAsync<TResponse>(RequestPacket message, CancellationToken token)
            where TResponse : ResponsePacket;
        public abstract Task<List<TResponse>> SendAsyncBroadcast<TResponse>(RequestPacket message, TimeSpan timeout, Action<uint, uint, uint, bool, TResponse> progressCallback, CancellationToken cancellationToken);

        public bool Matches(ClientFilter filter) {
            if (filter is null)
                return true;
            return filter.Matches(this);
        }

        public abstract void Dispose();

        public override string ToString() {
            return $"{this.GetType().Name}({Id})<{Name}>";
        }
    }
}
