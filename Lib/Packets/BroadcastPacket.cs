﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib.Packets
{
    [ProtoContract]
    [ProtoInclude(9001, typeof(ChannelBroadcast))]
    [ProtoInclude(9003, typeof(TypedChannelBroadcast))]
    public class BroadcastPacket : IPacket {
        [ProtoMember(1001)]
        public PacketType PacketType { get; set; }

        [ProtoMember(1002)]
        public ClientFilter ClientFilter { get; set; } = new ClientFilter();

        [ProtoMember(1003)]
        public uint SendingClientId { get; set; }

        public BroadcastPacket() { }

        public BroadcastPacket(ClientFilter filter) {
            if (filter is not null) {
                ClientFilter = filter;
            }
        }
    }
}
