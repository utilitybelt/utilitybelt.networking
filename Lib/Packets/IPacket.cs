﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib.Packets {
    /// <summary>
    /// All packets should inherit from this
    /// </summary>
    public interface IPacket {
        /// <summary>
        /// The type of packet this is. This is used for deserialization.
        /// </summary>
        public abstract PacketType PacketType { get; }
    }
}
