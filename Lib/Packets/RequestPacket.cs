﻿using ProtoBuf;
using System;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib.Packets {
    /// <summary>
    /// A request packet that expects a response.
    /// </summary>
    [ProtoContract(SkipConstructor = true)]
    [ProtoInclude(9001, typeof(PingRequest))]
    [ProtoInclude(9002, typeof(AuthRequest))]
    [ProtoInclude(9003, typeof(DummyRequest))]
    [ProtoInclude(9004, typeof(ChannelSubscriptionRequest))]
    [ProtoInclude(9005, typeof(ClientInfoRequest))]
    [ProtoInclude(9006, typeof(TypedBroadcastRequest))]
    public class RequestPacket : IPacket {
        [ProtoIgnore]
        internal static int _nextPacketId = 1;

        /// <summary>
        /// The id of this packet.
        /// </summary>
        [ProtoMember(1001)]
        public int PacketId { get; set; }

        /// <inheritdoc/>
        [ProtoMember(1002)]
        public PacketType PacketType { get; set; }

        /// <summary>
        /// Create a new request packet.
        /// </summary>
        public RequestPacket() {

        }

        /// <inheritdoc/>
        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
