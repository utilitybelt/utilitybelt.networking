﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Networking.Lib.Packets {

    /// <summary>
    /// Packet flags
    /// </summary>
    [Flags]
    public enum PacketFlags : uint {
        /// <summary>
        /// No Flags
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// Request Packet. This expects a return packet of type Response.
        /// </summary>
        Request = 0x00000001,

        /// <summary>
        /// Response packet. Send in reply to a Request packet.
        /// </summary>
        Response = 0x00000002,

        /// <summary>
        /// Broadcast packets are rebroadcast by the server based on the passed ClientFilter.
        /// </summary>
        Broadcast = 0x00000004
    }
}
