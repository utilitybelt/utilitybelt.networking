﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Networking.Lib.Packets {
    /// <summary>
    /// Packet types
    /// </summary>
    public enum PacketType : ushort {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// Ping request.
        /// </summary>
        PingRequest = 101,

        /// <summary>
        /// Ping response
        /// </summary>
        PingResponse = 102,

        /// <summary>
        /// A channel broadcast message
        /// </summary>
        ChannelBroadcast = 103,

        /// <summary>
        /// Auth request
        /// </summary>
        AuthRequest = 104,
        
        /// <summary>
        /// Auth response
        /// </summary>
        AuthResponse = 105,

        /// <summary>
        /// Dummy request, not used.
        /// </summary>
        DummyRequest = 106,

        /// <summary>
        /// Channel subscription request.
        /// </summary>
        ChannelSubscriptionRequest = 107,

        /// <summary>
        /// Channel subscription response
        /// </summary>
        ChannelSubscriptionResponse = 108,

        /// <summary>
        /// Client Info request
        /// </summary>
        ClientInfoRequest = 109,

        /// <summary>
        /// Client info response
        /// </summary>
        ClientInfoResponse = 110,

        /// <summary>
        /// Client info update
        /// </summary>
        ClientInfoUpdate = 111,

        /// <summary>
        /// Client event
        /// </summary>
        ClientEvent = 112,

        /// <summary>
        /// Typed broadcast
        /// </summary>
        TypedBroadcastRequest = 113,

        /// <summary>
        /// Typed channel broadcast
        /// </summary>
        TypedChannelBroadcast = 114,

        /// <summary>
        /// Result / Progress report from server when sending a TypedBroadcastRequest
        /// </summary>
        TypedBroadcastResult = 115,

        /// <summary>
        /// Response from a client replying to a TypedBroadcastRequest from the server
        /// </summary>
        TypedBroadcastResponse = 116
    }
}
