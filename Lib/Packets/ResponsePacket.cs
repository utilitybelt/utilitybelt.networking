﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib.Packets {
    /// <summary>
    /// This is sent in response to a RequestPacket.
    /// </summary>
    [ProtoContract]
    [ProtoInclude(9001, typeof(PingResponse))]
    [ProtoInclude(9002, typeof(AuthResponse))]
    [ProtoInclude(9003, typeof(ChannelSubscriptionResponse))]
    [ProtoInclude(9004, typeof(ClientInfoResponse))]
    [ProtoInclude(9005, typeof(TypedBroadcastResult))]
    [ProtoInclude(9006, typeof(TypedBroadcastResponse))]
    public class ResponsePacket : IPacket {
        /// <summary>
        /// The type of packet this is
        /// </summary>
        [ProtoMember(2001)]
        public PacketType PacketType { get; set; }

        /// <summary>
        /// The id of the request packet this is responding to.
        /// </summary>

        [ProtoMember(2002)]
        public int RequestPacketId { get; set; }

        /// <summary>
        /// Create a new ResponsePacket
        /// </summary>
        public ResponsePacket() { }

        /// <summary>
        /// Create a new Response packet from the specified request
        /// </summary>
        /// <param name="request"></param>
        public ResponsePacket(RequestPacket request) {
            RequestPacketId = request.PacketId;
        }

        /// <inheritdoc/>
        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
