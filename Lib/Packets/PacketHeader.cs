﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UtilityBelt.Networking.Lib.Packets {
    /// <summary>
    /// Packet Header
    /// </summary>
    [ProtoContract]
    public class PacketHeader {
        /// <summary>
        /// Protocol version
        /// </summary>
        [ProtoMember(1)]
        public int Version { get; set; } = 1;

        /// <summary>
        /// The type of packet in the body
        /// </summary>
        [ProtoMember(2)]
        public PacketType Type { get; set; } = PacketType.None;

        /// <summary>
        /// Flags for this packet
        /// </summary>
        [ProtoMember(3)]
        public PacketFlags Flags { get; set; } = PacketFlags.None;

        /// <summary>
        /// The id of the client that originally sent this packet.
        /// </summary>
        [ProtoMember(4)]
        public uint SendingClientId { get; set; }

        /// <summary>
        /// Get a string representation of the packet header
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return MessageSerializer.ToString(this);
        }
    }
}
