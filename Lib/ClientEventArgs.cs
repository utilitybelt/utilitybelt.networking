﻿using System;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib {
    public class ClientEventArgs : EventArgs {
        public ClientEventType EventType { get; }

        public RemoteClient Client { get; }

        public ClientEventArgs(RemoteClient client, ClientEventType eventType) {
            EventType = eventType;
            Client = client;
        }
    }
}