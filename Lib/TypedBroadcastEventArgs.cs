﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UtilityBelt.Networking.Lib.Packets;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Networking.Lib {
    /// <summary>
    /// Event Args for when a TypedBroadcast is received
    /// </summary>
    public class TypedBroadcastEventArgs : EventArgs {
        /// <summary>
        /// The broadcast that was sent
        /// </summary>
        public TypedBroadcastRequest Message { get; }

        internal TypedBroadcastEventArgs(TypedBroadcastRequest typedBroadcast) {
            Message = typedBroadcast;
        }

        /// <summary>
        /// Deserialize the message data as the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Deserialize<T>() {
            using (var stream = new MemoryStream(Message.Data)) {
                return Serializer.DeserializeWithLengthPrefix<T>(stream, PrefixStyle.Fixed32);
            }
        }

        /// <summary>
        /// Try to deserialize the message data as the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool TryDeserialize<T>(out T deserialized) where T : class {
            try {
                using (var stream = new MemoryStream(Message.Data)) {
                    deserialized = Serializer.DeserializeWithLengthPrefix<T>(stream, PrefixStyle.Fixed32);
                }
                return true;
            }
            catch {
                deserialized = null;
                return false;
            }
        }

        /// <summary>
        /// Check if this message data is of the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsType(Type type){
            return $"{type.Namespace}.{type.Name}" == Message.Type;
        }
    }
}
