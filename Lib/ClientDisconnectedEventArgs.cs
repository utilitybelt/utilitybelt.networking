﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Networking.Lib {
    public enum DisconnectReason {
        ClosedLocally,
        Aborted,
        CloseRemotely
    }

    public class ClientDisconnectedEventArgs : EventArgs {
        public ITransportClient Client { get; }

        public DisconnectReason Reason { get; }

        public ClientDisconnectedEventArgs(ITransportClient client, DisconnectReason reason) {
            Client = client;
            Reason = reason;
        }
    }
}
