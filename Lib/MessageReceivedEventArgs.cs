﻿using System;
using UtilityBelt.Networking.Lib.Packets;

namespace UtilityBelt.Networking.Lib
{
    public class MessageReceivedEventArgs : EventArgs {
        public ITransportClient Client { get; }
        public PacketHeader PacketHeader { get; }
        public IPacket Packet { get; }

        public MessageReceivedEventArgs(ITransportClient client, PacketHeader packetHeader, IPacket packet) {
            Client = client;
            PacketHeader = packetHeader;
            Packet = packet;
        }
    }
}