﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UtilityBelt.Networking.Lib {
    public abstract class IListenServer : IDisposable {
        public string Host { get; }
        public int Port { get; }

        public abstract bool IsListening { get; }

        public abstract event EventHandler<ClientConnectedEventArgs> OnClientConnected;
        public abstract event EventHandler<ClientDisconnectedEventArgs> OnClientDisconnected;
        public abstract event EventHandler<MessageReceivedEventArgs> OnMessageReceived;

        public IListenServer(string host, int port) {
            Host = host;
            Port = port;
        }

        public abstract Task RunAsync(CancellationToken? token = null);

        public abstract void Stop();

        public abstract void Dispose();
    }
}
